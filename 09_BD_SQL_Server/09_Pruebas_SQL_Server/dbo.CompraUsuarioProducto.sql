﻿CREATE TABLE [dbo].[CompraUsuarioProducto] (
    [IdUsuario]  INT      NOT NULL,
    [IdProducto] INT      NOT NULL,
    [Fecha]      DATETIME NOT NULL,
    [Cantidad]   TINYINT  NOT NULL,
    CONSTRAINT [PK_CompraUsuarioProducto] PRIMARY KEY CLUSTERED ([IdUsuario] ASC, [IdProducto] ASC, [Fecha] ASC),
	FOREIGN KEY (IdUsuario) REFERENCES Usuario(Id),
	FOREIGN KEY (IdProducto) REFERENCES Producto(Id)
);

