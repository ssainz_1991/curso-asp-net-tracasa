﻿-- Para evitar duplicados
SELECT DISTINCT edad FROM [dbo].[Usuario]
SELECT COUNT (DISTINCT edad) FROM [dbo].[Usuario]

select * from Usuario where edad > 25
select * from Usuario where activo = 'True'
select * from Usuario where activo = 1 and altura < 1.7
select * from Usuario where activo = 0 or edad >= 30
select * from Usuario where not(activo = 0 or edad >= 30)

SELECT * FROM Usuario ORDER BY edad DESC, activo ASC

SELECT * FROM Usuario where coche is null
SELECT * FROM Usuario where coche is not null

-- Devolver los 5 usuarios de mayor edad sin coche
SELECT * FROM Usuario ORDER BY edad DESC
SELECT TOP 5 * FROM Usuario WHERE coche is null ORDER BY edad DESC

-- Devolver los 5 usuarios de mayor edad sin coche,
-- ordenados alfabeticamente por nombre: Necesaria subconsulta
SELECT * FROM (
SELECT TOP 5 * FROM Usuario WHERE coche is null ORDER BY edad DESC
) As subconsulta_5 order by nombre

-- Devolver cuantos usuarios hay inactivos y cuantos activos
Select count (activo) from Usuario where activo = 1 Union
Select count (activo) from Usuario where activo = 0

select activo, count (activo) from usuario group by activo

SELECT COUNT(DISTINCT AC.ID) n_activos, COUNT(DISTINCT INAC.ID) n_inactivos
FROM USUARIO AC, USUARIO INAC
WHERE AC.activo = 1 AND INAC.activo = 0

-- Devuelve los 3 usuarios de menor altura ordenados por edad
SELECT * FROM (
SELECT TOP 3 * FROM Usuario ORDER BY altura ASC
) As subconsulta order by edad

-- Devuelve los usuarios con el mismo coche
select * from Usuario as U1, Usuario as U2 where U1.coche = U2.coche AND U1.Id < U2.Id
order by U1.nombre

-- Devolver el usuario de mas edad con coche
SELECT MAX(edad) FROM Usuario WHERE coche is not null -- No devuelve el usuario
SELECT TOP 1 * FROM Usuario WHERE coche is not null ORDER BY edad DESC

-- Devolver el usuario de menos edad inactivo y sin coche
SELECT MIN(edad) FROM Usuario where activo = 'False' and coche is null -- No devuelve el usuario
SELECT TOP 1 * FROM Usuario where activo = 'False' and coche is null ORDER BY edad ASC

SELECT COUNT(*) AS cantidad,
		CASE WHEN coche is not null then coche ELSE 'Sin coche' END
		AS coche
		FROM Usuario
		GROUP BY coche
		ORDER BY cantidad

-- Calcula la media de edades de los usuarios activos, inactivos, con coche y sin coche
SELECT 'Activo' AS Estado, AVG(CAST(edad AS FLOAT)) AS Edad_Media FROM Usuario WHERE activo = 1 UNION
SELECT 'Inactivo', AVG(CAST(edad AS FLOAT)) FROM Usuario WHERE activo = 0 UNION
SELECT 'Con coche', AVG(CAST(edad AS FLOAT)) FROM Usuario WHERE coche is not null UNION
SELECT 'Sin coche', AVG(CAST(edad AS FLOAT)) FROM Usuario WHERE coche is null

SELECT AVG(CASE WHEN activo = 1 THEN CAST(edad AS FLOAT) END) AS activos, 
   	   AVG(CASE WHEN activo = 0 THEN CAST(edad AS FLOAT) END) AS inactivos,
	   AVG(CASE WHEN coche is not null THEN CAST(edad AS FLOAT) END) AS con_coche,
	   AVG(CASE WHEN coche is null THEN CAST(edad AS FLOAT) END) AS sin_coche
FROM Usuario

SELECT U.Id, U.nombre FROM Usuario as U WHERE U.nombre LIKE 'S%'
SELECT U.Id, U.nombre, U.email FROM Usuario as U WHERE U.email LIKE '%@g%'