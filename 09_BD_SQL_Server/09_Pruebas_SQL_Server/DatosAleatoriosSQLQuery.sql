﻿-- SELECT * FROM Usuario AS U WHERE U.tipoVia LIKE 'Av%' AND U.nombreVia = 'Europa'
DECLARE @Count AS INT
DECLARE @Usu AS INT
DECLARE @Prod AS INT
DECLARE @FechaStr AS VARCHAR(100)
DECLARE @Fecha AS DATETIME
DECLARE @Cantidad AS INT

DELETE FROM CompraUsuarioProducto WHERE 1 = 1;
SET @Count = 0
WHILE @Count < 20
BEGIN
	SET @Count = @Count + 1
	SET @Usu = 5 + RAND(CHECKSUM(NEWID())) * 10
	SET @Prod = 1 + RAND(CHECKSUM(NEWID())) * 5
	SET @Cantidad = 1 + RAND(CHECKSUM(NEWID())) * 100
	SET @FechaStr = CAST(ROUND(2018 + RAND(CHECKSUM(NEWID())) * 2, 0) AS VARCHAR)
			+ '-' + CAST(ROUND(1 + RAND(CHECKSUM(NEWID())) * 11, 0) AS VARCHAR)
			+ '-' + CAST(ROUND(1 + RAND(CHECKSUM(NEWID())) * 27, 0) AS VARCHAR)
	SET @Fecha = CAST(@FechaStr AS DATETIME)
	-- SET @Fecha = dateadd(day, (abs(CHECKSUM(newid())) % 3650) * -1, getdate());
	-- set @Fecha = DATEADD(DAY,ABS(CHECKSUM(NEWID()))% (1+DATEDIFF(DAY,@fechafin,@fechaini)),@fechaini)
	INSERT INTO CompraUsuarioProducto (IdUsuario, IdProducto, Fecha, Cantidad)
			VALUES (@Usu, @Prod, @Fecha, @Cantidad)
	PRINT 'ID Usu ' + CAST(@Usu AS VARCHAR) 
		+ ' - ' + 'ID Prod ' + CAST(@Prod AS VARCHAR) 
		+ ' - ' + 'Cantidad ' + CAST(@Cantidad AS VARCHAR)
		 + ' - ' + 'Fecha ' + CAST(@FechaStr AS VARCHAR)
END;
PRINT 'Final bucle';