﻿/*-- INNER JOIN saca solo los datos en común de las dos tablas
SELECT U.Id, U.email, C.Id, C.Marca FROM Usuario AS U INNER JOIN Coche AS C
	ON U.idCoche = C.Id
-- LEFT JOIN saca todos los registros de la primera tabla y los coincidentes de la segunda y si no tiene datos le pone NULL
SELECT U.Id, U.email, C.Id, C.Marca FROM Usuario AS U LEFT JOIN Coche AS C
	ON U.idCoche = C.Id
-- RIGHT JOIN saca todos los registros de la segunda tabla y los coincidentes de la primera y si no tiene datos le pone NULL
SELECT U.Id, U.email, C.Id, C.Marca FROM Usuario AS U RIGHT JOIN Coche AS C
	ON U.idCoche = C.Id
-- FULL OUTER JOIN saca todos los registros de las dos tablas, tengan coincidentes o no
SELECT U.Id, U.email, C.Id, C.Marca FROM Usuario AS U FULL OUTER JOIN Coche AS C
	ON U.idCoche = C.Id*/

/*
	SELECT SUM(CUP.Cantidad) FROM Producto as P, CompraUsuarioProducto as CUP, Usuario as U 
		WHERE CUP.IdUsuario = U.Id AND CUP.IdProducto = P.Id AND U.edad <= 25
		1 - Esta consulta con JOIN
		
	Por cada usuario, nombre de usuario y nombre de producto,
		2 - El precio del producto de sus compras
		3 - Cuanto se ha gastado en total en cada producto
		4 - Cuantas copias del producto ha comprado
	Por otro lado:
		5 - Todas las ventas (dinero) del 2020
		6 - Todos los usuarios que compraron en el 2019
		7 - Qué productos se han vendido mejor a lo largo del 2018, 2019 y 2020
		8 - El coche cuyo propietario ha comprado más a lo largo de la historia
		9 - La media de edad de los compradores de cada uno de los productos (melón, cuajada...)
		10 - Qué productos han comprado los usuarios del Mercedes
*/
-- SELECT SUM(CUP.Cantidad) FROM Producto as P, CompraUsuarioProducto as CUP, Usuario as U 
--		WHERE CUP.IdUsuario = U.Id AND CUP.IdProducto = P.Id AND U.edad <= 25
--	1 - Esta consulta con JOIN
SELECT SUM(CUP.Cantidad) FROM Producto AS P 
RIGHT JOIN CompraUsuarioProducto AS CUP ON P.Id = CUP.IdProducto
LEFT JOIN Usuario AS U ON CUP.IdUsuario = U.Id
WHERE U.edad <= 25

--	Por cada usuario, nombre de usuario y nombre de producto,
--		2 - El precio del producto de sus compras
SELECT DISTINCT U.Id, U.nombre Usuario, P.Nombre Producto, P.Precio 
FROM Producto AS P 
RIGHT JOIN CompraUsuarioProducto AS CUP ON P.Id = CUP.IdProducto
LEFT JOIN Usuario AS U ON CUP.IdUsuario = U.Id
GROUP BY U.Id, U.nombre, P.Nombre, P.Precio
ORDER BY U.nombre
--		3 - Cuanto se ha gastado en total en cada producto
SELECT U.Id, U.nombre Usuario, P.Nombre Producto, SUM(P.Precio*CUP.Cantidad) Gastado 
FROM Producto AS P 
RIGHT JOIN CompraUsuarioProducto AS CUP ON P.Id = CUP.IdProducto
LEFT JOIN Usuario AS U ON CUP.IdUsuario = U.Id
GROUP BY U.Id, U.nombre, P.Nombre
ORDER BY U.nombre
--		4 - Cuantas copias del producto ha comprado
SELECT U.Id, U.nombre Usuario, P.Nombre Producto, SUM(CUP.Cantidad) Copias_Compradas 
FROM Producto AS P 
RIGHT JOIN CompraUsuarioProducto AS CUP ON P.Id = CUP.IdProducto
LEFT JOIN Usuario AS U ON CUP.IdUsuario = U.Id
GROUP BY U.Id, U.nombre, P.Nombre
ORDER BY U.nombre
--Por otro lado:
--		5 - Todas las ventas (dinero) del 2020
-- Ventas por producto
SELECT P.Nombre Producto, SUM(CUP.Cantidad*P.Precio) Ventas 
FROM Producto AS P 
RIGHT JOIN CompraUsuarioProducto AS CUP ON P.Id = CUP.IdProducto
WHERE CUP.Fecha LIKE '%2020%'
GROUP BY P.Nombre
-- Total de ventas
SELECT SUM(CUP.Cantidad*P.Precio) Ventas 
FROM Producto AS P 
RIGHT JOIN CompraUsuarioProducto AS CUP ON P.Id = CUP.IdProducto
WHERE YEAR(CUP.Fecha) = '2020'
--		6 - Todos los usuarios que compraron en el 2019
SELECT DISTINCT U.Id, U.nombre Usuario FROM CompraUsuarioProducto AS CUP
LEFT JOIN Usuario AS U ON CUP.IdUsuario = U.Id
WHERE YEAR(Fecha) = 2019
--		7 - Qué productos se han vendido mejor a lo largo del 2018, 2019 y 2020
-- Año y Cantidad, pero falta nombre del producto
SELECT A.Año, MAX(A.CantidadYear) AS Cantidad FROM (
SELECT P.Nombre, YEAR(CUP.Fecha) Año, SUM(CUP.Cantidad) CantidadYear  
FROM Producto AS P 
INNER JOIN CompraUsuarioProducto AS CUP ON P.Id = CUP.IdProducto
INNER JOIN Usuario AS U ON CUP.IdUsuario = U.Id
GROUP BY P.Nombre, YEAR(CUP.Fecha)
) AS A
GROUP BY A.Año
-- Consultas por año unidas
SELECT * FROM (
SELECT TOP 1 P.Nombre, YEAR(CUP.Fecha) Año, SUM(CUP.Cantidad) CantidadYear  FROM Producto AS P 
INNER JOIN CompraUsuarioProducto AS CUP ON P.Id = CUP.IdProducto
INNER JOIN Usuario AS U ON CUP.IdUsuario = U.Id
WHERE YEAR(CUP.Fecha) = '2018'
GROUP BY P.Nombre, YEAR(CUP.Fecha) ORDER BY CantidadYear DESC
) AS A 
UNION ALL
SELECT * FROM (
SELECT TOP 1 P.Nombre, YEAR(CUP.Fecha) Año, SUM(CUP.Cantidad) CantidadYear  FROM Producto AS P 
INNER JOIN CompraUsuarioProducto AS CUP ON P.Id = CUP.IdProducto
INNER JOIN Usuario AS U ON CUP.IdUsuario = U.Id
WHERE YEAR(CUP.Fecha) = '2019'
GROUP BY P.Nombre, YEAR(CUP.Fecha) ORDER BY CantidadYear DESC 
) AS B 
UNION ALL
SELECT * FROM (
SELECT TOP 1 P.Nombre, YEAR(CUP.Fecha) Año, SUM(CUP.Cantidad) CantidadYear  FROM Producto AS P 
INNER JOIN CompraUsuarioProducto AS CUP ON P.Id = CUP.IdProducto
INNER JOIN Usuario AS U ON CUP.IdUsuario = U.Id
WHERE YEAR(CUP.Fecha) = '2020'
GROUP BY P.Nombre, YEAR(CUP.Fecha) ORDER BY CantidadYear DESC 
) AS C 
-- Con Transact-SQL
DECLARE @ANO AS INT
SET @ANO = 2018
WHILE @ANO <= 2020
BEGIN

SELECT TOP 1 Producto.Id, Producto.Nombre, sum(cantidad) as copias_vendidas, year(CompraUsuarioProducto.Fecha) as periodo from CompraUsuarioProducto inner join Producto on CompraUsuarioProducto.IdProducto = Producto.Id
group by Producto.Id, Producto.Nombre, year(CompraUsuarioProducto.Fecha) HAVING year(CompraUsuarioProducto.Fecha) = @ANO ORDER BY copias_vendidas DESC

SET @ANO = @ANO +1

END;
--		8 - El coche cuyo propietario ha comprado más a lo largo de la historia
-- Muestra el usuario con más compras y con coche, si no tiene coche muestra la siguiente
SELECT C.Marca, C.Modelo FROM
(SELECT U.idCoche , SUM(CUP.Cantidad) Cantidad FROM Producto AS P 
INNER JOIN CompraUsuarioProducto AS CUP ON P.Id = CUP.IdProducto
INNER JOIN Usuario AS U ON CUP.IdUsuario = U.Id
GROUP BY U.nombre, U.idCoche) AS A
INNER JOIN Coche AS C ON A.idCoche = C.Id
WHERE Cantidad = (SELECT MAX(A.Cantidad) FROM
(SELECT U.nombre, U.idCoche , SUM(CUP.Cantidad) Cantidad FROM Producto AS P 
INNER JOIN CompraUsuarioProducto AS CUP ON P.Id = CUP.IdProducto
INNER JOIN Usuario AS U ON CUP.IdUsuario = U.Id
GROUP BY U.nombre, U.idCoche) AS A
INNER JOIN Coche AS C ON A.idCoche = C.Id)
GROUP BY C.Marca, C.Modelo
-- Otra manera que no muestra si el usuario no tiene coche
select Usuario.Id, Usuario.nombre, Coche.Marca, Coche.Modelo 
from Usuario inner join coche on Usuario.idCoche= coche.Id 
where Usuario.Id in(
select top 1 CompraUsuarioProducto.IdUsuario 
from CompraUsuarioProducto group by IdUsuario order by sum(cantidad) desc)
--Otra manera que muestra null si el que más compras tiene no tiene coche
SELECT TOP 1 C.Marca,C.Modelo
FROM ((Producto as P INNER JOIN CompraUsuarioProducto as UP ON P.Id = UP.idProducto ) RIGHT JOIN Usuario as U ON U.Id=UP.idUsuario) LEFT JOIN Coche as C on C.id=U.idCoche
GROUP BY U.Id,C.Marca,C.Modelo
ORDER BY SUM(P.precio*UP.cantidad) DESC
--Y otra que hace lo mismo
SELECT C.Marca, C.Modelo FROM (SELECT TOP 1 U.Id , SUM(P.Precio*C.Cantidad) AS 'PrecioTotal' 
FROM Producto AS P INNER JOIN CompraUsuarioProducto AS C 
		ON P.Id = C.IdProducto INNER JOIN Usuario AS U ON U.Id = C.IdUsuario WHERE U.idCoche IS NOT NULL 
		GROUP BY U.Id 
		ORDER BY PrecioTotal DESC) AS S, Coche AS C 
		INNER JOIN Usuario AS U ON C.Id =U.idCoche WHERE U.Id = S.Id
--		9 - La media de edad de los compradores de cada uno de los productos (melón, cuajada...)
-- Hay que hacerlo con subconsulta para que no repita edades de usuarios repetidos
SELECT subconsulta.nombreProd, AVG(subconsulta.edad) AS Media 
FROM (
	SELECT DISTINCT U.edad as edad, U.Id , CUP.IdProducto as idprod, Producto.Nombre AS nombreProd
	FROM Usuario U 
	INNER JOIN CompraUsuarioProducto CUP ON U.Id = CUP.IdUsuario
	INNER JOIN Producto ON Producto.Id = CUP.IdProducto
) AS subconsulta 
GROUP BY subconsulta.nombreProd
-- No es del todo correcto porque coge las edades varias veces si el usuario ha comprado más de una vez el mismo producto
SELECT P.Nombre Producto, AVG(CAST(U.Edad AS FLOAT)) Edad_Media_Compradores 
FROM Producto AS P 
RIGHT JOIN CompraUsuarioProducto AS CUP ON P.Id = CUP.IdProducto
LEFT JOIN Usuario AS U ON CUP.IdUsuario = U.Id
GROUP BY P.Nombre
-- Lo mismo pero muestra todos los productos y pone 0 cuando no tiene compras. No es del todo correcto
SELECT P.Nombre Producto, 
CASE WHEN AVG(CAST(U.Edad AS FLOAT)) IS NOT NULL
	THEN AVG(CAST(U.Edad AS FLOAT))
	ELSE 0
END Edad_Media_Compradores 
FROM Producto AS P 
FULL OUTER JOIN CompraUsuarioProducto AS CUP ON P.Id = CUP.IdProducto
LEFT JOIN Usuario AS U ON CUP.IdUsuario = U.Id
GROUP BY P.Nombre
-- Con Transact-SQL y no es del todo correcto
DECLARE @aux_id INT
DECLARE cur_u CURSOR FOR SELECT P.Id FROM Producto AS P

OPEN cur_u

FETCH NEXT FROM cur_u INTO @aux_id

WHILE @@FETCH_STATUS = 0
	BEGIN
		SELECT IdProducto, AVG(Edad) as edad_media
		FROM Usuario INNER JOIN CompraUsuarioProducto ON Usuario.Id = CompraUsuarioProducto.IdUsuario
		GROUP BY IdProducto HAVING IdProducto = @aux_id
		FETCH NEXT FROM cur_u INTO @aux_id
	END
CLOSE cur_u
DEALLOCATE cur_u
--		10 - Qué productos han comprado los usuarios del Mercedes
SELECT U.Id, U.Nombre, P.Nombre Producto FROM Producto AS P 
RIGHT JOIN CompraUsuarioProducto AS CUP ON P.Id = CUP.IdProducto
LEFT JOIN Usuario AS U ON CUP.IdUsuario = U.Id
LEFT JOIN Coche AS C ON U.idCoche = C.Id
WHERE C.Marca = 'Mercedes'
GROUP BY U.Id, U.Nombre, P.Nombre