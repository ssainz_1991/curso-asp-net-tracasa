﻿DECLARE @aux_id INT
DECLARE cur_u CURSOR FOR SELECT U.Id FROM Usuario AS U

OPEN cur_u

FETCH NEXT FROM cur_u INTO @aux_id

WHILE @@FETCH_STATUS = 0
	BEGIN
		SELECT SUM(C.Cantidad), (SELECT nombre FROM Usuario WHERE Usuario.Id = @aux_id)  
			FROM CompraUsuarioProducto AS C WHERE C.IdUsuario = @aux_id
		FETCH NEXT FROM cur_u INTO @aux_id
	END

CLOSE cur_u
DEALLOCATE cur_u
