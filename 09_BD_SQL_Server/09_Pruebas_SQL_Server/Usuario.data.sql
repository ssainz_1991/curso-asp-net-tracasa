﻿SET IDENTITY_INSERT [dbo].[Usuario] ON
INSERT INTO [dbo].[Usuario] ([email], [nombre], [edad], [altura], [activo]) VALUES (N'sergio30@hotmail.com', N'Sergio', 30, 1.83, 1)
INSERT INTO [dbo].[Usuario] ([email], [nombre], [edad], [altura], [activo]) VALUES (N'carlos30@gmail.com', N'Carlos', 30, 1.76, 1)
INSERT INTO [dbo].[Usuario] ([email], [nombre], [edad], [altura], [activo]) VALUES (N'maria26@hotmail.es', N'Maria', 26, 1.68, 1)
INSERT INTO [dbo].[Usuario] ([email], [nombre], [edad], [altura], [activo]) VALUES (N'susana28@gmail.com', N'Susana', 28, 1.65, 1)
INSERT INTO [dbo].[Usuario] ([email], [nombre], [edad], [altura], [activo]) VALUES (N'jesus28@hotmail.com', N'Jesus', 28, 1.58, 1)
INSERT INTO [dbo].[Usuario] ([email], [nombre], [edad], [altura], [activo]) VALUES (N'miguel23@gmail.com', N'Miguel', 23, 1.68, 1)
INSERT INTO [dbo].[Usuario] ([email], [nombre], [edad], [altura], [activo]) VALUES (N'jose_luis54@hotmail.es', N'Jose Luis', 54, 1.65, 1)
INSERT INTO [dbo].[Usuario] ([email], [nombre], [edad], [altura], [activo]) VALUES (N'javier21@hotmail.com', N'Javier', 21, 1.76, 1)
INSERT INTO [dbo].[Usuario] ([email], [nombre], [edad], [altura], [activo]) VALUES (N'maria23@gmail.com', N'Maria', 23, 1.74, 1)
INSERT INTO [dbo].[Usuario] ([email], [nombre], [edad], [altura], [activo]) VALUES (N'claudia28@hotmail.es', N'Claudia', 28, 1.78, 1)
SET IDENTITY_INSERT [dbo].[Usuario] OFF
