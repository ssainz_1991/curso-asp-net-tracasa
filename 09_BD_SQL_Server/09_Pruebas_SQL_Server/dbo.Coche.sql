﻿CREATE TABLE [dbo].[Coche] (
    [Id]     INT          IDENTITY (1, 1) NOT NULL,
    [Marca]  VARCHAR (50) NOT NULL,
    [Modelo] VARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

