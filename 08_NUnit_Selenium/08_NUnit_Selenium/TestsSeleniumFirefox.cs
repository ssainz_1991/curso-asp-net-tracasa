using NUnit.Framework;
using System.IO;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;

namespace _08_NUnit_Selenium
{
    public class TestsSeleniumFirefox
    {
        IWebDriver driver;

        [OneTimeSetUp]
        public void InicializarClaseTest()
        {
            // string fichFirefox = "../../../../FirefoxPortable/FirefoxPortable.exe";
            // string fichFirefox = "C:\\Users\\pmpcurso1\\AppData\\Local\\Mozilla Firefox\\Firefox.exe";
            string fichFirefox = "../../../../FirefoxPortable/App/Firefox64/Firefox.exe";
            if (! File.Exists(fichFirefox))
            {
                string instalador = "../../../../FirefoxPortable_92.0_English.paf.exe";
                if (File.Exists(instalador))
                {
                    System.Diagnostics.Process.Start(instalador);
                }
            }
            if (File.Exists(fichFirefox))
            {
                // string rutaDriverFirefox = "geckodriver.exe";
                // FirefoxBinary binarioFirefox = new FirefoxBinary(fichFirefox);
                FirefoxOptions firefoxOptions = new FirefoxOptions();
                firefoxOptions.BrowserExecutableLocation = fichFirefox;
                driver = new FirefoxDriver(firefoxOptions);
            }
        }
        [OneTimeTearDown]
        public void FinalizarClaseTest()
        {
            // driver.Close();
        }
        [SetUp]
        public void Setup()
        {
           
        }

        [Test]
        public void Test1()
        {
            driver.Navigate().GoToUrl("https://duckduckgo.com/");
            IWebElement textoBusq = driver.FindElement(By.Name("q"));
            textoBusq.SendKeys("SQL Tutorials w3school create table");
            IWebElement botonBusq = driver.FindElement(By.Id("search_button_homepage"));
            botonBusq.Click();
            var enlaces = driver.FindElements(By.CssSelector("a[href*='https://www.w3schools.com/']"));
            foreach (var enlace in enlaces)
            {
                if (enlace.Equals(enlaces[0]) && enlace.Displayed)
                {
                    enlace.Click();
                    break;
                }
            }
            Assert.GreaterOrEqual(enlaces.Count, 3, " No se han encontrado enlaces");
            driver.FindElement(By.Id("accept-choices")).Click();

            Actions accion = new Actions(driver);
            var enlaceSQL = driver.FindElement(By.CssSelector("a[href='sql_datatypes.asp']"));
            accion.MoveToElement(enlaceSQL);
            /*for (int i = 0; i < 20; i++)
                accion.KeyDown(Keys.ArrowDown);*/
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].scrollIntoView(true);", enlaceSQL);
            Wait(3);
            enlaceSQL.Click();

            IWebElement tituloNumDT = driver.FindElement(By.XPath("//h3[text() = 'Numeric Data Types'][2]"));
            Assert.IsNotNull(tituloNumDT, "No se han encontrado los tipos de datos numericos");
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].scrollIntoView(true);", tituloNumDT);
        }
        [Test]
        public void TestDataType()
        {
            driver.Navigate().GoToUrl("https://www.w3schools.com/sql/sql_datatypes.asp");
            driver.FindElement(By.Id("accept-choices")).Click();
            IWebElement tituloNumDT = driver.FindElement(By.XPath("//h3[text() = 'Numeric Data Types'][2]"));
            Assert.IsNotNull(tituloNumDT, "No se han encontrado los tipos de datos numericos");
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].scrollIntoView(true);", tituloNumDT);
            
            var columDataType = driver.FindElements(By.XPath("/html/body/div[7]/div[1]/div[1]/div[8]/table/tbody/tr/td[1]"));
            var columStorage = driver.FindElements(By.XPath("/html/body/div[7]/div[1]/div[1]/div[8]/table/tbody/tr/td[3]"));

            Assert.AreEqual(columDataType.Count, columStorage.Count, "No cuadran las columnas de la tabla");
            Assert.GreaterOrEqual(columDataType.Count, 5, "No son suficientes las columnas de la tabla");
            Assert.AreEqual(columDataType[columStorage.Count - 1].Text, "real", "La ultima fila debe ser 'real'");
            int i = 0;
            foreach (var element in columDataType)
            {
                Console.WriteLine(element.Text + " " + columStorage[i].Text);
                i++;
            }
        }
        public void Wait(int seg, int timeOut = 60)
        {
            var wait = new WebDriverWait(driver, new TimeSpan(0, 0, 1, timeOut));
            var delay = new TimeSpan(0, 0, 0, seg);
            var timeInic = DateTime.Now;
            wait.Until(driver => (DateTime.Now - timeInic) > delay);
        }
    }
}