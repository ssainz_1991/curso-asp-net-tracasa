﻿using Ejemplo01_Encapsulacion;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio02_Herencia
{ 
    public class Empleado : Ejercicio01_Encapsulacion.Usuario
    {
        float salario;
        Coche sucoche;

        public Coche SuCoche
        {
            get { return sucoche; }
            set { sucoche = value; }
        }

        public Empleado() : base("", 0, 0)
        {
            
        }

        public Empleado(string nombre, int edad, float altura, float salario) 
            : base(nombre, edad, altura)
        {
            this.Salario = salario;
        }

        public float Salario { get => salario; 
            set => salario = (value < 7000) ? 7000 : value ;
        }

        public override string ToString()
        {
            return base.ToString().Replace("Usuario", "Empleado") 
                + ", " + this.Salario + " EUR"
                + (SuCoche == null ? "" : ". Posee coche: " + SuCoche);
        }
        public override void PedirDatos()
        {
            base.PedirDatos();
            UIConsole.PedirNum("Salario", out salario);
        }
    }
}
