﻿using Ejemplo01_Encapsulacion;
using Ejemplo02_Herencia;
using Ejemplo03_Interfaces;
using Ejercicio01_Encapsulacion;
using Ejercicio02_Herencia;
using System;

/* 1 - Comprobar si Coche Electrico puede hacerse polimorfismo con INombrale.
 * 2 - Crear un nuevo proyecto Ejercicio03_Interfaces:
 *      Hacer que el usuario que ya tenemos implemente la interfaz INombrable
 *      y usar los 2 métodos y la propiedad
 * 3 - Crear un array de INombrables con un empleado y su coche eléctrico.
 * 4 - Implementar la interfaz ICloneable en CocheElectrico:
 *      El método tiene que instanciar un nuevo obj y asignar las propiedades del nuevo coche con las suyas propias.
 * 5 - Crear una nueva interfaz que obligue a implementar 2 métodos, uno para mostrar
 *      directamente los datos por consola y otro para pedir sus datos por consola.
 * 6 - Implementar dicha interfaz en Usuario y en Coche, sobreescribir los métodos en Empleado (si quereis en CocheElectrico).
 * 7 - Usar los métodos en un nuevo usuario, en el empleado del ejercicio 3 y en un Coche (y si quereis en CocheElectrico).
 */
namespace Ejercicio03_Interfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("INombrable");
            INombrable[] varios = new INombrable[2];
            varios[0] = new Empleado("Miguel Angel", 38, 1.83f, 25000);
            varios[1] = new CocheElectrico("BMW", "S2", 18500, 90);
            //Completamos la dependencia unidireccional
            ((Empleado)varios[0]).SuCoche = (Coche)varios[1];
            foreach (INombrable inom in varios)
            {
                Console.WriteLine(inom.ToString());
            }

            CocheElectrico mismoElectrico = (CocheElectrico) varios[1];
            CocheElectrico copiaElectrico = (CocheElectrico) ((CocheElectrico)varios[1]).Clone();

            mismoElectrico.Modelo = "Nuevo modelo S2";
            Console.WriteLine("mismoElectrico = " + mismoElectrico.ToString());
            Console.WriteLine("varios[1] = " + varios[1].ToString());

            copiaElectrico.Modelo = "Copia nuevo modelo S2";
            Console.WriteLine("copiaElectrico = " + copiaElectrico.ToString());
            Console.WriteLine("varios[1] = " + varios[1].ToString());

            Empleado empleadoUsuario = new Empleado();
            empleadoUsuario.PedirDatos();
            empleadoUsuario.MostrarDatos();

            Coche cocheUsuario = new Coche();
            cocheUsuario.PedirDatos();
            cocheUsuario.MostrarDatos();
        }
    }
}
