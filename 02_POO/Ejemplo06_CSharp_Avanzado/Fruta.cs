﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo06_CSharp_Avanzado
{
    public class Fruta
    {
        string nombre;
        float pesoMedio;
        public string Nombre { get => nombre; set => nombre = value; }
        public float PesoMedio { get => pesoMedio; set => pesoMedio = value; }
        public Fruta(string nombre, float pesoMedio)
        {
            Nombre = nombre ?? throw new ArgumentNullException(nameof(nombre));
            PesoMedio = pesoMedio;
        }
        public override string ToString()
        {
            return "Fruta " + Nombre + ", peso medio = " + PesoMedio + " gr";
        }
    }
}
