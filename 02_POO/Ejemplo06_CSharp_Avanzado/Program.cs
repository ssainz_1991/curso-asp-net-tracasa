﻿using Ejemplo06_CSharp_Avanzado.MetodosExt;
using System;

namespace Ejemplo06_CSharp_Avanzado
{
    class Program
    {
        static void Main(string[] args)
        {
            Fruta esLaPera = new Fruta("La pera repera", 150.01f);
            Console.WriteLine(esLaPera.ToString());
            Console.WriteLine(esLaPera.FormatearNombre());
            Console.WriteLine(OtroModulo.FormatearNombreN(esLaPera));
            Console.WriteLine(esLaPera.FormatearNombre("Texto formato"));
            Console.WriteLine(esLaPera.ToUpperString());
            object otroObjeto = new object();
            Console.WriteLine(otroObjeto.ToUpperString());
            int[] otroArray = new int[] { 3, 2, 1 };
            Console.WriteLine(otroArray.ToUpperString());
            string[] otroArrayStr = new string[] { "tres", "dos", "uno" };
            Console.WriteLine(otroArrayStr.ToUpperString());
            Fruta[] arrayFruta = new Fruta[] {esLaPera, new Fruta ("La manzana roja", 120.50f), new Fruta("La naranja mecanica", 180.80f) };
            Console.WriteLine(arrayFruta.ToUpperString());
            Console.WriteLine(arrayFruta.ToStringFlitrado("roja"));
        }
    }
    // En C# podemos añadir métodos a una clase desde otras clases:
    public static class OtroModulo
    {
        public static string FormatearNombreN(Fruta laFruta)
        {
            return "FRUTA " + laFruta.Nombre.ToUpper();
        }
    }
}
