﻿using System;
using System.Collections.Generic;
using System.Text;

// Ejercicio: Método de extensión de array, que sea array.ToStringFlitrado(string cadena);
// Que sólo muestre los ToString() que contengan cadena, sin tener en cuenta mayus o minus.

namespace Ejemplo06_CSharp_Avanzado.MetodosExt
{
    public static class ModuloMetodosExtension
    {
        // Mediante una función estática, con esta estructura:
        public static string FormatearNombre(this Fruta laFruta)
        {
            // Esto es un Método de Extensión
            return "FRUTA " + laFruta.Nombre.ToUpper();
        }
        public static string FormatearNombre(this Fruta laFruta, string formato)
        {
            // Esto es un Método de Extensión 
            return formato + " " + laFruta.Nombre.ToUpper();
        }
        public static string ToUpperString(this object cualquierObjeto)
        {
            return cualquierObjeto.ToString().ToUpper();
        }
        public static string ToUpperString(this Array cualquierArray)
        {
            string result = "Array " + cualquierArray.ToString().ToUpper();
            foreach (object elem in cualquierArray)
            {
                result += "\n - " + elem.ToUpperString();
            }
            return result;
        }
        public static string ToStringFlitrado(this Array cualquierArray, string cadenaFiltro)
        {
            string result = "Array flitrado " + cualquierArray.ToString().ToUpper();
            foreach (object elem in cualquierArray)
            {
                if (elem.ToString().ToUpper().Contains(cadenaFiltro.ToUpper()))
                result += "\n - " + elem.ToString();
            }
            return result;
        }
    }
}
