﻿using System;

namespace Ejemplo04_Excepciones
{
    class Program
    {
        static void FuncionRecursivaInfinita(int i)
        {
            if (i < 100)
            FuncionRecursivaInfinita(i++);
        }
        static void Main(string[] args)
        {
            // Excepciones típicas existentes en .Net (y en otros lenguajes)
            // Stack Overflow:
            try
            {
                // No queda otra que evitarlo
                FuncionRecursivaInfinita(200);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("ERROR!! " + ex.Message);
            }
            // CAPTURAR ERROR DE MANERA ESPECIFICA (FormatException)
            try
            {
                Console.WriteLine("Convirtiendo lo inconvertible!");
                int.Parse("NO NUMERO");
                Console.WriteLine("Esta linea NI SE MUESTRA");
            }
            catch (FormatException ex) // Cuando ocurre una excepción el control pasa a los bloques catch
            {
                Console.Error.WriteLine("ERROR!! " + ex.Message);
            }
            // CAPTURAR ERROR DE MANERA GENÉRICA (Exception)
            // Como Exception es el padre, puede capturar todo tipo de excepciones
            try
            {
                Console.WriteLine("Convirtiendo lo inconvertible!");
                int.Parse("NO NUMERO");
                Console.WriteLine("Esta linea NI SE MUESTRA");
            }
            catch (Exception ex) // Cuando ocurre una excepción el control pasa a los bloques catch
            {
                Console.Error.WriteLine("ERROR!! " + ex.Message);
            }
            // CAPTURAR OTRO ERROR DE MANERA GENÉRICA 
            try
            {
                Console.WriteLine("Crear un objeto nulo y tratar de usarlo");
                object nadaYmenos = null;
                Console.WriteLine(nadaYmenos.ToString());
            }
            catch (NullReferenceException ex) // Cuando ocurre una excepción el control pasa a los bloques catch
            {
                Console.Error.WriteLine("ERROR!! " + ex.Message);
            }
            // NO CAPTURAR OTRO ERROR
            try
            {
                Console.WriteLine("Crear un objeto nulo y tratar de usarlo");
                object nadaYmenos = null;
                Console.WriteLine(nadaYmenos.ToString());
            }
            catch (FormatException ex) // Cuando ocurre una excepción el control pasa a los bloques catch
            {
                Console.Error.WriteLine("ERROR!! " + ex.Message);
            }
            catch (SystemException ex) // Cuando ocurre una excepción el control pasa a los bloques catch
            {
                Console.Error.WriteLine("ERROR!! " + ex.Message);
            }
            catch (Exception ex) // Cuando ocurre una excepción el control pasa a los bloques catch
            {
                Console.Error.WriteLine("ERROR!! " + ex.Message);
            }
            // Provocar una excepción con throw
            try
            {
                throw new FormatException("Error de formato porque me da la gana");
            }
            catch (FormatException ex) // Cuando ocurre una excepción el control pasa a los bloques catch
            {
                Console.Error.WriteLine("ERROR!! " + ex.Message);
            }

            try
            {
                FuncionQueDelegaExcepcion();
                // La excepción se propaga siempre hacia arriba hasta que un try catch adecuado la controle
            }
            catch (FormatException ex) // Cuando ocurre una excepción el control pasa a los bloques catch
            {
                Console.Error.WriteLine("ERROR!! " + ex.Message);
            }
        }

        public static void OtraFuncionQueDelegaExcepcion()
        {
            // Si no se controla, se propaga hacia arriba
            try { FuncionQueDelegaExcepcion(); } catch (NullReferenceException ex) { }        
        }
        public static void FuncionQueDelegaExcepcion()
        {
            // Si una función lanza una excepción, y no la controlamos aquí,
            // podrá o deberá ser controlada en la función que invoca a esta  
            int.Parse("ERROR");
        }
    }
}
