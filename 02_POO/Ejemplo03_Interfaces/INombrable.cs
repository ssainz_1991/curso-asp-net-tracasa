﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo03_Interfaces
{
    /*
     * Esta comentada porque no podemos hacer referencias cruzadas entre proyectos.
     * Entre objetos(clases) si se puede hacer, pero tampoco se debe, porque
     * aumenta mucho la complejidad y lo que se llama DEUDA TÉCNICA.
     * Por eso comentamos esta interfaz y solo usamos la de Ejemplo01_Encapsulación
     * 
    interface INombrable
    {
        string GetNombre();

        void SetNombre(string unNombre);

        string Nombre
        {
            get;
            set;
        }
    }
    */
}
