﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo03_Interfaces
{
    /* Como un contrato que las clases que la implementen tienen que cumplir
     * Cualquier clase que implemente la interfaz (NO se dice herede),
     * debe tener programados todos sus métodos y propiedades.
     * Las interfaces privadas (como las clases privadas) solo se pueden
     * usar dentro del proyecto (aplicación, librería DLL...)
     * Si son públicas puedes usarse fuera del proyecto
     */
    public interface INombrable
    {
        string GetNombre();

        void SetNombre(string unNombre);

        string Nombre
        {
            get;
            set;
        }

        // A priori no se debe, las interfaces por definición son contratos, NO clases
        void MetodoQueSea()
        {
            Console.WriteLine("Hacer cosas");
        }
    }
}
