﻿using Ejemplo01_Encapsulacion;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo02_Herencia
{
    /*Ejercicio: Clase CocheElectrico añadiendo campo nivelBateria
     double
    Al acelerar, debemos también reducir la batería */
    public class CocheElectrico : Coche, ICloneable
    {
        public CocheElectrico()
        {
            NivelBateria = 100;
            Marca = "Una asiática";
            Modelo = "Casero";
        }

        public CocheElectrico(string marca, string modelo, float precio)
            : base(marca, modelo, precio)
        {
            NivelBateria = 100;
        }
        // Sobrecarga de constructores
        public CocheElectrico(string marca, 
            string modelo, 
            float precio,
            double nivelBat)
            : base(marca, modelo, precio)
        {
            NivelBateria = nivelBat;
        }
        double nivelBateria;

        public double NivelBateria {
            get { return nivelBateria; }
            set { nivelBateria = value; }
        }
        // Con new, ignoramos que sea
        // un método virtual
        // y si es un método normal,
        // casi es obligatorio usarlo
        new public void Acelerar()
        {
            base.Acelerar();
            nivelBateria -= 0.1;
        }
        // Sobrecarga de métodos cuando usamos el mismo nombre
        // de método pero con diférente combinación de param
        // En realidad, son métodos diferentes
        public void Acelerar(float gastoBateria)
        {
            base.Acelerar();
            nivelBateria -= gastoBateria;
        }
        // Podemos hacer tantos métodos como queramos
        public void AcelerarConGasto(float gastoBateria)
        {
            base.Acelerar();
            nivelBateria -= gastoBateria;
        }
        // override = sobreescritura de métodos
        // Con override, machacamos el método del padre y siempre
        // usaremos nuestro método, aunque el objeto tenga la forma del padre
        public override string ToString()
        {
            return "Coche electrico: " + base.Marca + 
                ", " + this.Modelo + "(" + base.ToString() + ")";
        }

        public object Clone()
        {
            // return new CocheElectrico(this.Marca, this.Modelo, precio, NivelBateria);
           
            CocheElectrico nuevo = new CocheElectrico();
            nuevo.Marca = Marca;
            nuevo.Modelo = Modelo;
            nuevo.Precio = precio;
            nuevo.NivelBateria = NivelBateria;
            return nuevo;
        }
    }
}