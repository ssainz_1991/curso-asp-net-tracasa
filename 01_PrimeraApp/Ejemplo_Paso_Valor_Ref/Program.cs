﻿using System;

namespace Ejemplo_Paso_Valor_Ref
{
    class Program
    {
        static void Main(string[] args)
        {
            int variableEnt = 10;

            Console.WriteLine("Entero fuera y antes: " + variableEnt);
            RecibimosUnValor(variableEnt); //modifica el valor de la variable pero cuando termina no le cambia el valor
            Console.WriteLine("Entero fuera y despues: " + variableEnt);

            Console.WriteLine("Entero fuera y antes ref: " + variableEnt);
            RecibimosUnaRef(ref variableEnt); //modifica el valor al entrar, pero también el de la variable que hemos indicado al llamar a la función
            Console.WriteLine("Entero fuera y despues ref: " + variableEnt);

            string frase1 = "La piscina esta llena.";
            Console.WriteLine("Frase: " + frase1);
            ConvertirMayus(ref frase1);
            Console.WriteLine("Frase en mayusculas: " + frase1);
        }
        static void RecibimosUnValor(int entero)
        {
            Console.WriteLine("Entero dentro y antes: " + entero);
            entero = entero * 2 + 1;
            Console.WriteLine("Entero dentro y despues: " + entero);
        }
        static void RecibimosUnaRef(ref int entero)
        {
            Console.WriteLine("Entero dentro y antes ref: " + entero);
            entero = entero * 2 + 1;
            Console.WriteLine("Entero dentro y despues ref: " + entero);
        }
        static void ConvertirMayus(ref string frase)
        {
            frase = frase.ToUpper();
        }
    }
}
