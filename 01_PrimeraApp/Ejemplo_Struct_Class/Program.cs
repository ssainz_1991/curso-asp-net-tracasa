﻿using System;

namespace Ejemplo_Struct_Class
{
    /*
    Crear una estrucutra ProductoE independiente (NO anidada)
    con nombre y precio, y su constructor
    con una función para mostrar sus datos

    Crear una clase ProductoC independiente (NO anidada)
    con nombre y precio, y su constructor
    con una función para mostrar sus datos

    Crear 4 funciones estaticas en Program:
        - Una que reciba ProductoE y modifique su nombre y precio
        - Otra que reciba una ref ProductoE y modifique su nombre y precio
        - Una que reciba ProductoC y modifique su nombre y precio
        - Otra que reciba una ref ProductoC y modifique su nombre y precio

    Por último, en Main(), comprobar el comportamiento para saber
    cual modifica realmente la variable original
    */

    
        public struct ProductoE
    {
        /* string es una clase especial que se comporta más como tipo primitivo*/
        public string nombre;
        /*Los tipos primitivos en realidad son estructuras*/
        public float precio;

        public ProductoE(string nombre, float precio)
        {
            this.nombre = nombre;
            this.precio = precio;
        }

        public void MostrarProductoE(ProductoE p)
        {
            Console.WriteLine("Nombre: " + p.nombre + "   Precio: " + p.precio);
        }
    /* 1 - Las estructuras siempre se pasan por valor a menos que se indique a posta con ref que es por referencia
     * 2 - No pueden heredar unas de otras
     * 3 - No pueden haber estructuras estáticas
     * 4 - Siempre siempre tienen un constructor por defecto
     */
    }
    public class ProductoC
    {
        public string nombre;
        public float precio;

        public ProductoC(string nombre, float precio)
        {
            this.nombre = nombre;
            this.precio = precio;
        }
    /* 1 - Las clase siempre se pasan por referencia. Es redundante usar ref
     * 2 - Sí pueden heredar unas clases de otras
     * 3 - Sí pueden haber clases estáticas
     * 4 - Sólo tienen constructor por defecto cuando no hemos creado uno explícitamente con algún argumento
     */
        public void MostrarProductoC(ProductoC p)
        {
            Console.WriteLine("Nombre: " + p.nombre + "   Precio: " + p.precio);
        }
        static void ModProductoE_Valor(ProductoE p)
        {
            p.nombre = "Sofa";
            p.precio = 258.68f;
        }
        static void ModProductoE_Ref(ref ProductoE p)
        {
            p.nombre = "Armario";
            p.precio = 89.45f;
        }
        static void ModProductoC_Valor(ProductoC p)
        {
            p.nombre = "Taburete";
            p.precio = 12.5f;
        }
        static void ModProductoC_Ref(ref ProductoC p)
        {
            p.nombre = "Alfombra";
            p.precio = 110.67f;
        }
        static void Main(string[] args)
        {
            ProductoE productoE = new ProductoE("Mesa", 45.8f);
            ProductoC productoC = new ProductoC("Silla", 26.5f);

            Console.WriteLine("Datos originales estructura");
            productoE.MostrarProductoE(productoE);
            Console.WriteLine("Datos modificados por valor estructura");
            ModProductoE_Valor(productoE);
            productoE.MostrarProductoE(productoE);
            Console.WriteLine("Datos modificados por referencia estructura");
            ModProductoE_Ref(ref productoE);
            productoE.MostrarProductoE(productoE);
            Console.WriteLine("Datos originales clase");
            productoC.MostrarProductoC(productoC);
            Console.WriteLine("Datos modificados por valor clase");
            ModProductoC_Valor(productoC);
            productoC.MostrarProductoC(productoC);
            Console.WriteLine("Datos modificados por referencia clase");
            ModProductoC_Ref(ref productoC);
            productoC.MostrarProductoC(productoC);
        }
    }
}
