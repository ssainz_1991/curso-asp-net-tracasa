﻿using System;

namespace Ejemplo10_For
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 0; i < 10; i++)
                Console.WriteLine("Num" + i);
            // Por ejemplo, podemos suarlo como while porque es un while
            bool salir = false;
            for (Console.WriteLine("Emepezamos!"); ! salir; salir = bool.Parse(Console.ReadLine()))
            {
                Console.WriteLine("¿Salimos?");
            }
            //Ejercicio 6
            int cuadrante1, cuadrante2, cuadrante3, cuadrante4, n, f, x, y;
            cuadrante1 = 0; cuadrante2 = 0; cuadrante3 = 0; cuadrante4 = 0;
            string linea;
            Console.Write("Cuantos valores ingresará: ");
            linea = Console.ReadLine();
            n = int.Parse(linea);
            for (f = 1; f <= n; f++)
            {
                Console.Write("Introduce la coordenada (x): ");
                linea = Console.ReadLine();
                x = int.Parse(linea);
                Console.Write("Introduce las coordenada (y): ");
                linea = Console.ReadLine();
                y = int.Parse(linea);
                if (x >= 0 && y >= 0)
                {
                    cuadrante1 = cuadrante1 + 1;
                }
                else
                    if (x >= 0 && y < 0)
                {
                    cuadrante2 = cuadrante2 + 1;
                }
                else
                    if (x < 0 && y >= 0)
                {
                    cuadrante4 = cuadrante4 + 1;
                }
                else
                    cuadrante3 = cuadrante3 + 1;

            }
            Console.WriteLine("La cantidad de valores ingresados en el cuadrante 1 es: ");
            Console.Write(cuadrante1);
            Console.WriteLine("La cantidad de valores ingresados en el cuadrante 2 es: ");
            Console.Write(cuadrante2);
            Console.WriteLine("La cantidad de valores ingresados en el cuadrante 3 es: ");
            Console.Write(cuadrante3);
            Console.WriteLine("La cantidad de valores ingresados en el cuadrante 4 es: ");
            Console.Write(cuadrante4);
            //Otra forma
            {
                Console.WriteLine("Cuantos? ");
                int c = int.Parse(Console.ReadLine());
                int c1, c2, c3, c4;
                c1 = c2 = c3 = c4 = 0;

                for (int i = 0; i < c; i++)
                {
                    Console.WriteLine("X? ");
                    x = int.Parse(Console.ReadLine());
                    Console.WriteLine("X? ");
                    y = int.Parse(Console.ReadLine());

                    c1 += (x >= 0 && y >= 0) ? 1 : 0;
                    c2 += (x >= 0 && y < 0) ? 1 : 0;
                    c3 += (x < 0 && y < 0) ? 1 : 0;
                    c4 += (x < 0 && y >= 0) ? 1 : 0;
                }
            }
        }
       
    }
}
