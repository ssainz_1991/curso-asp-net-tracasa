﻿using System;

namespace Ejemplo12_TratamientoCadenas
{
    class Program
    {
        static void Main(string[] args)
        {
            string texto = "     En un lugar de La Mancha de cuyo nombre me quiero acordar, pero no me acuerdo.      ";
            Console.WriteLine("Original: " + texto);
            Console.WriteLine("Sin espacios: " + texto.Trim());
            Console.WriteLine("Mayus: " + texto.ToUpper());
            Console.WriteLine("Minus: " + texto.ToLower());
            Console.WriteLine("Un cacho: " + texto.Substring(20, 20)); //(Desde donde, cantidad)
            Console.WriteLine("Un cacho hasta el final: " + texto.Substring(20));
            Console.WriteLine("La Mancha?: " + texto.IndexOf("La Mancha"));
            Console.WriteLine("LA MANCHA?: " + texto.ToUpper().IndexOf("LA MANCHA"));
            Console.WriteLine("Pamplona?: " + texto.IndexOf("Pamplona"));

            string[] palabras = texto.Trim().Split(" ");
            Console.WriteLine("Palabra por palabra: ");
            for (int p = 0; p < palabras.Length; p++)
            {
                Console.WriteLine("Palabra " + p + ": " + palabras[p]);
            }

            Console.WriteLine("Por Pamplona: " + texto.Replace("La Mancha", "Pamplona"));
            Console.WriteLine("Por Pamplona y Sarriguren: "
                + texto.Replace("La Mancha", "Pamplona")
                .Replace("no", "Sarriguren"));

            /*//Ejercicio: Pedir al usuario 3 palabras y juntarlas en una única variable tipo texto, separándolas por comas y sin espacios
            string palabra1, palabra2, palabra3;
            Console.WriteLine("Introduce la primera palabra: ");
            palabra1 = Console.ReadLine();
            Console.WriteLine("Introduce la segunda palabra: ");
            palabra2 = Console.ReadLine();
            Console.WriteLine("Introduce la tercera palabra: ");
            palabra3 = Console.ReadLine();
            string cadena = palabra1.Trim() + "," + palabra2.Trim() + "," + palabra3.Trim();
            Console.WriteLine(cadena);*/
            //Otra forma
            Console.WriteLine("\n\n");
            palabras = new string[3];
            for (int p = 0; p < palabras.Length; p++)
            {
                Console.WriteLine("Escribe palabra " + p + ": ");
                palabras[p] = Console.ReadLine().Trim();
                string sus = palabras[p];
                //while (sus != palabras[p].Replace("  ", " "));
                while (sus.IndexOf("  ") >= 0)
                {
                    sus = palabras[p].Replace("  ", " ");
                    palabras[p] = sus;
                }
            }
            string resultado = String.Join(",", palabras);
            Console.WriteLine("Resultado: " + resultado);

            //Otra forma sin depurar
            string[] texto2 = new string[3];
            for (int i = 0; i < texto.Length; i++)
            {
                string palabra;
                Console.Write("Escribe una palabra: ");
                palabra = Console.ReadLine();
                palabra.Trim();
                texto2[i] = String.Join(" ", palabra.Split(" ", StringSplitOptions.RemoveEmptyEntries));
            }
            string resultado2 = String.Join(",", texto2);
            Console.WriteLine(resultado2);

            //Forma más eficiente
            Console.WriteLine("Escriba la primera palabra");
            string palabra1 = (Console.ReadLine());
            Console.WriteLine("Escriba la segunda palabra");
            string palabra2 = (Console.ReadLine());
            Console.WriteLine("Escriba la tercera palabra");
            string palabra3 = (Console.ReadLine());
            Console.WriteLine("Escriba la cuarta palabra");
            string palabra4 = (Console.ReadLine());

            string[] palabras3 = 
            {
                palabra1.Replace("  ", string.Empty),
                palabra2.Replace("  ", string.Empty),
                palabra3.Replace("  ", string.Empty),
                palabra4.Replace("  ", string.Empty)
            };
            var resultado3 = string.Join(", ", palabras3);
            Console.WriteLine("Resultado:" + resultado3);
        }
    }
}
