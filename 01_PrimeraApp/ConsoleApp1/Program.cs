﻿using System;

namespace ConsoleApp1
{ 
   class Program
    {
        static void Main(string[] args)
        {
            int numero = 10;
            // string str_num = 10;
            // numero = "ahora eres un string";
            var str_num = "Un texto";
            str_num = 109.ToString();

            Console.WriteLine("Hola mundo! " + numero);
            if (args.Length > 0)
            {
                Console.WriteLine("Hola mundo! " + args[0]);
            }
        }
    }
}
