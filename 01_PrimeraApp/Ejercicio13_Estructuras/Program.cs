﻿using System;

namespace Ejercicio13_Estructuras
{
    //Crear una estructura Usuario con nombre (texto), edad (entero) y altura (decimal)
    //Crear 2 variables usuarios
    //Crear un array usuarios con 4 usuarios, los 2 de antes y 2 más que pidamos por teclado
    //Crear un método MostrarUsuario que muestre sus datos
    //Al terminar, mostrar los 4 usuarios.
        public struct usuario
        {
            public string nombre;
            public int edad;
            public float altura;
            public usuario(string nombre, int edad, float altura)
            {
                this.nombre = nombre;
                this.edad = edad;
                this.altura = altura;
            }
            public void MostrarUsuario()
            {
             Console.WriteLine("Nombre: " + nombre);
             Console.WriteLine("Edad: " + edad);
             Console.WriteLine("Altura: " + altura);
            }
        }
    class Program
    {
        static void Main(string[] args)
        {
            usuario usuario1 = new usuario("Claudia", 28, 168);
            usuario usuario2 = new usuario("Maria Jesús", 32, 172);

            usuario[] usuarios = new usuario[4];

            usuarios[1] = usuario1;
            usuarios[2] = usuario2;

            for (int cont = 2; cont < usuarios.Length; cont++)
            {
                Console.Write("Ingrese el nombre:");
                string nombre = Console.ReadLine();
                string linea;
                Console.Write("Ingrese la edad:");
                linea = Console.ReadLine();
                int edad = int.Parse(linea);
                Console.Write("Ingrese la altura:");
                linea = Console.ReadLine();
                float altura = float.Parse(linea);

                usuarios[cont] = new usuario(nombre, edad, altura);
                Console.WriteLine();
            }
            for (int cont = 0; cont < usuarios.Length; cont++)
            {
                usuarios[cont].MostrarUsuario();
            }
        }
    }

    //Otra forma
/*
    public struct Usuario
    {
        public string nombre;
        public int edad;
        public float altura;

        public Usuario(string nombre, int edad, float altura)
        {
            this.nombre = nombre;
            this.edad = edad;
            this.altura = altura;
        }

        public void MostrarUsuario()
        {
            Console.WriteLine("");
            Console.WriteLine("=====================================================");
            Console.WriteLine("Nombre: " + nombre + ".");
            Console.WriteLine("Edad:   " + edad.ToString() + " Años");
            Console.WriteLine("Altura: " + altura.ToString() + "m.");
            Console.WriteLine("=====================================================");
        }
    }

    class Program1
    {
        static void Main(string[] args)
        {
            Usuario[] personas = new Usuario[4];

            personas[0] = new Usuario("Pijus Magnificus", 22, 175);
            personas[1] = new Usuario("Incontinencia Suma", 25, 164);

            for (int cont = 2; cont < personas.Length; cont++)
            {
                Console.Write("Introduzca el nombre: ");
                string nombre = Console.ReadLine();

                Console.Write("Introduzca la edad: ");
                int edad = int.Parse(Console.ReadLine());

                Console.Write("Introduzca la altura (en M.): ");
                float altura = float.Parse(Console.ReadLine());
                personas[cont] = new Usuario(nombre, edad, altura);
            }

            for (int cont = 0; cont < personas.Length; cont++)
            {
                personas[cont].MostrarUsuario();
            }

        }

    }*/

}
