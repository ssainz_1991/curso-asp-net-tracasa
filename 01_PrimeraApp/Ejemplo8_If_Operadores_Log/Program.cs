﻿using System;

namespace Ejemplo8_If_Operadores_Log
{
    class Program
    {
        static void Main(string[] args)
        {
            int max = int.MinValue, min = int.MaxValue, num1;
            int cantidad = 5, contador =0;
            string linea;
            while (contador < cantidad)
            { 
            Console.Write("Ingrese valor:");
            linea = Console.ReadLine();
            num1 = int.Parse(linea);
            if (num1 > max) max = num1;
                //if (num1 < min) min = num1;
                min = num1 < min ? num1 : min;
                /*if (num1 < min)
                    min = num1;
                else
                    min = min;*/
                contador++;
            }
            Console.WriteLine("Max: " + max);
            Console.WriteLine("Min: " + min);
            Console.ReadKey();
        }
    }
}
