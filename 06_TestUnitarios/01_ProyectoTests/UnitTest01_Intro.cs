using NUnit.Framework;
using NUnit.Framework.Constraints;
using System;

namespace _01_ProyectoTests
{
    public class Tests
    {
        string texto;
        // Este es el m�todo de inicializaci�n porque lleva el atributo C# [SetUp]
        // Los atributos son "decoradores", caracter�sticas que a�adimos a clases, propiedades y m�todos
        // Para agregar cierta funcionalidad
        [SetUp]
        public void Inicializacion()
        {
            texto = "Texto inicial";
        }

        [TearDown]
        public void Cleanup()
        {
            texto = "Clean";
        }
        [Test]
        public void TestPrimero()
        {
            // Assert = asegurar
            Assert.Pass();
            Assert.AreEqual("Clean", texto);
            texto = "Texto inicial 1";
        }

        [Test]
        public void TestSegundo()
        {
            Assert.AreEqual(1 + 3, 2 + 2);
            Assert.AreNotEqual(1 + 3, 2 + 0);
            Assert.IsNotNull(this.texto, "Texto es nulo");
            Assert.AreNotEqual("Clean", texto, "El texto no se ha limpiado correctamente");
            texto = "Texto inicial 2";
        }
        public void Probando_isTrue_Contains()
        {
            Console.WriteLine("Antes del test 3");
            Assert.IsTrue(texto.Equals("Texto inicial"), "El texto no se ha inicializado");
            Assert.Contains('i', texto.ToCharArray(), "El array de char texto no tiene 'i'");
        }
            public static void DelegadoCualquieraOk()
        {
            Console.WriteLine("Delegado Cualquiera Ok");
        }
        public static void DelegadoCualquieraMal()
        {
           throw new NotImplementedException("Delegado Cualquiera Mal");
        }

        [Test(Author ="German", Description = "Probando diferencias entre Assert.Throws y Assert.Catch")]
        public void Probando_Excepciones_Assert_ThrowsCatch()
        {
            TestDelegate delegateOk = DelegadoCualquieraOk;
            Assert.DoesNotThrow(delegateOk, "Delegado Cualquiera Ok FALL�");

            Exception ex = Assert.Throws<NotImplementedException>(DelegadoCualquieraMal, "Delegado Cualquiera Mal CASC�");
            Console.WriteLine("NotImplementedException: " + ex.Message);
            // Falla el test porque Throws no contempla la herencia
            // ex = Assert.Throws<Exception>(DelegadoCualquieraMal, "Delegado Cualquiera Mal CASC� con Exception");


            ex = Assert.Catch<Exception>(DelegadoCualquieraMal, "Delegado Cualquiera Ok CASC�");
            ex = Assert.Catch<NotImplementedException>(DelegadoCualquieraMal, "Delegado Cualquiera Ok CASC�");

            Console.WriteLine("Despu�s del test 3");
        }
        [Test(Author = "German", Description = "Test Que Falla A Proposito")]
        public void TestQueFallaAProposito()
        {
            if ( texto.Equals("LKjlkjlkjkl"))
                Assert.Fail("Fallo adrede");
        }
    }
}