﻿using NUnit.Framework;
using NUnit.Framework.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_ProyectoTests
{
    [TestFixture]
    public class AssertThrowsTests
    {
        [Test(Author = "Ramón RL", Description = "Recoger una excepción asíncrona")]
        public void TestThrowsAsync()
        {
            // Using a method as a delegate
            Assert.ThrowsAsync<ArgumentException>(async () => MethodThatThrows());
            // Assert.Multiple(async () => await MethodThatThrows());
            Console.WriteLine("CINCO");
        }
        async Task MethodThatThrows()
        {
            Console.WriteLine("UNO");
            await Task.Delay(3000);
            Console.WriteLine("DOS");
            var t = Task.Run(delegate
            {
                Task.Delay(2000);
                Console.WriteLine("TRES");
            });
            //t.Wait();
            Console.WriteLine("CUATRO");
            throw new ArgumentException();
        }

        [Test(Author = "juaned", Description = "estudiando el funcionamiento de async")]
        public void TestExplicandoAsync()
        {
            // si descomentamos esto el test fallara aqui y ya no ejecutara nada mas
            // Assert.AreEqual(2 + 5, 5, "cuenta antes de ThrowsAsync");

            AssertionException ex = Assert.ThrowsAsync<AssertionException>(async () => await MethodThatThrowsJuan());

            Console.WriteLine(ex.ResultState.Status);
            if (ex.ResultState.Status == TestStatus.Failed)
            {
                Assert.AreEqual(2 + 5, 5, "cuenta mal 1");
            }
            else
            {
                Assert.AreEqual(2 + 5, 5, "cuenta mal 2");
            }
        }

        async Task MethodThatThrowsJuan()
        {
            await Task.Delay(5000);
            // Assert.AreEqual(1 + 5, 4, "mal dentro de MethodThatThrows");
            AssertionException ex = new AssertionException("boom");
            throw ex;
        }

    }
}
