﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using Ejemplo03_MVC.DLL.Modelo;

namespace Ejemplo03_MVC.Test
{
    class TestModeloEjemplo
    {
        IModeloEjemplo model1;

        [SetUp]
        public void Inicialización()
        {
            model1 = new ModeloEjemploLista();
        }
        [Test(Author = "Sergio S", Description = "Probando Modelo Ejemplo")]
        public void TestModeloListaEjemplo_Crear()
        {
            Assert.NotNull(model1.Crear(1, "Uno"));
            Assert.NotNull(model1.Crear(2, "Dos"));
            Assert.NotNull(model1.Crear(3, "Tres"));

            Assert.Null(model1.Crear(4, null));
            Assert.Null(model1.Crear(5, ""));

            Assert.AreEqual(model1.LeerTodos().Count, 3);
        }
        [Test(Author = "Sergio S", Description = "Probando Modelo Ejemplo")]
        public void TestModeloListaEjemplo_Modificar()
        {
            TestModeloListaEjemplo_Crear();
            Assert.NotNull(model1.Modificar("Uno", 1, "Uno nuevo"));
            Assert.Null(model1.Modificar("Uno que no existe", 1, "Uno nuevo"));
            Assert.Catch<Exception>(() => model1.Modificar(null));

            Assert.AreEqual(model1.LeerTodos().Count, 3);
        }
    }
}
