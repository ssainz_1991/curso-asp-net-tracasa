using NUnit.Framework;
using ModeloUsuarios;
using System;
using System.Reflection;
using System.Collections.Generic;

namespace TestModeloUsuario
{
    public class TestsControladorYModelosADO
    {
        ModeloUsuario modelo;
        ModuloPersistenciaADO moduloPersistencia;
        Usuario usu1;
        int auxId;
        int posicionInicio;
        List<Usuario> listaAux;

        /*void LimpiarJSON()
        {

        }*/

        // Eliminar los usuarios creados durante la prueba
        void Eliminar_Usuarios_Validos(int i)
        {
            int indice = posicionInicio + 1;
            while (moduloPersistencia.Leer().Count > posicionInicio+1)
            {
                modelo.Eliminar((int)moduloPersistencia.Leer()[indice].Id);
            }
        }
        void ResetearModeloYCtrl_ConReflection ()
        {
            // FieldInfo[] camposModelo = ModeloUsuario.Instancia.GetType().GetFields(...);
            FieldInfo[] camposModelo = typeof(ModeloUsuario).GetFields(
                BindingFlags.NonPublic | BindingFlags.Static);
            foreach (FieldInfo campo in camposModelo)
            {
                if (campo.Name.Equals("instance"))
                {
                    ModeloUsuario.Instancia.ToString();
                    Console.WriteLine("Encontrado instance Modelo y eliminado");
                    campo.SetValue(null, null);
                    ModeloUsuario.Instancia.ToString();
                }
            }
            moduloPersistencia = new ModuloPersistenciaADO(Entorno.Desarrollo);
            modelo = ModeloUsuario.Instancia;
            //  type.GetField("instance", BindingFlags.NonPublic | BindingFlags.Static).SetValue(null, null);
        }
        void EliminarTodosUsuarios()
        {
            // var lista = ModeloUsuario.Instancia.LeerTodos();

            while (ModeloUsuario.Instancia.LeerTodos().Count > 0)
            {
                ModeloUsuario.Instancia.Eliminar(0);
            }
            /*while (ModeloUsuario.Instancia.LeerTodos().Count > 0)
            {
                var primero = ModeloUsuario.Instancia.LeerTodos()[0];
                ModeloUsuario.Instancia.Eliminar(primero.Nombre);
            }*/
        }
        [OneTimeSetUp]

        public void OneTimeSetUp()
        {
            listaAux = new List<Usuario>();
            moduloPersistencia = new ModuloPersistenciaADO(Entorno.Desarrollo);
            listaAux = moduloPersistencia.Leer();
            if(moduloPersistencia.IdUltimo() != null)
            {
                auxId = (int)moduloPersistencia.IdUltimo();
                posicionInicio = listaAux.Count - 1;
            }
        }

        [SetUp]
        public void Setup()
        {
            //moduloPersistencia = new ModuloPersistenciaADO(Entorno.Desarrollo);
            modelo = ModeloUsuario.Instancia;
        }

        [TearDown]
        public void AlTerminarTest()
        {
            // ResetearModeloYCtrl_ConReflection();
            Eliminar_Usuarios_Validos(auxId);
        }
        
        void CrearUsuariosValidos()
        {
            usu1 = new Usuario("hola", 27, 1.5f);
            modelo.Crear(usu1);
            modelo.Crear(new Usuario("adios", 33, 2));
            modelo.Crear(new Usuario("qtal", 45, 2));
        }
           
        [Test]
        public void TestCrearValidos()
        {
            CrearUsuariosValidos();
            modelo.Crear(new Usuario("adios2", -3, 2.2f));
            modelo.Crear(new Usuario("qtal2", 400, 27));
            Assert.AreEqual(1, modelo.LeerUno(posicionInicio + 4).Edad);
            Assert.AreEqual(1.0f, modelo.LeerUno(posicionInicio + 5).Altura);
            Assert.AreEqual("hola", modelo.LeerUno(posicionInicio + 1).Nombre);
            Assert.Null(modelo.LeerUno(posicionInicio + 7));
            Assert.AreEqual(modelo.LeerTodos().Count, posicionInicio + 6, "Mal creados los usuarios");
            Assert.IsTrue(modelo.LeerUno(posicionInicio + 1).Equals(new Usuario("hola", 27, 1.5f)));
        }

        [Test]
        public void TestCrearInvalidos()
        {
            modelo.Crear(new Usuario(null, 400, 27));
            modelo.Crear(null);
            Assert.AreEqual(modelo.LeerTodos().Count, posicionInicio + 1, "Mal creados los usuarios");
        }

            [Test]
        public void TestModificar()
        {
            CrearUsuariosValidos();
            Usuario usu1mod = new Usuario("hola", -3, 7);//Te lo cambia aqu� directamente
            usu1mod.Id = modelo.LeerTodos()[posicionInicio + 1].Id;
            modelo.Modificar(usu1mod);
            Assert.AreNotEqual(modelo.LeerUno(posicionInicio + 1), usu1);
            Assert.AreEqual(modelo.LeerUno(posicionInicio + 1), usu1mod);
            Usuario usuNoExiste = new Usuario("NoExisto",88 ,88 );
            modelo.Modificar(usuNoExiste);
            for(int i=0;i< modelo.LeerTodos().Count; i++)
            {
                Assert.AreNotEqual(modelo.LeerUno(i), usuNoExiste);
            }
        }
       

        [Test]
        public void TestEliminar()
        {
            Usuario usu1 = new Usuario("hola", 1, 1);
            modelo.Crear(usu1);
            modelo.Crear(new Usuario("adios", 43, 2));
            modelo.Crear(new Usuario("qtal", 40, 2));
            Usuario usu = modelo.leerTodo()[posicionInicio + 2];
            Assert.AreEqual(true, modelo.Eliminar((int)usu.Id));
            foreach(Usuario usuario in modelo.LeerTodos())
            {
                Assert.AreEqual(false, usuario == usu);
            }
            Assert.AreEqual(posicionInicio + 3, modelo.LeerTodos().Count);
            Assert.AreEqual(false, modelo.Eliminar(-10));
            Assert.AreEqual(posicionInicio + 3, modelo.LeerTodos().Count);
        }

        [Test]
        public void TestLeerUno()
        {
            Usuario usu1 = new Usuario("hola", 1, 1);
            modelo.Crear(usu1);
            modelo.Crear(new Usuario("adios", 43, 2));
            modelo.Crear(new Usuario("qtal", 40, 2));
            Assert.Null(modelo.LeerUno(posicionInicio + 5));
            Assert.NotNull(modelo.LeerUno(posicionInicio + 2));
            Assert.IsInstanceOf<Usuario>(modelo.LeerUno(posicionInicio + 2));
       
        }
        
        [Test]
        public void TestLeerTodos()
        {
            Assert.AreEqual(posicionInicio + 1, modelo.LeerTodos().Count);
            modelo.Crear(new Usuario("adios", 43, 2));
            modelo.Crear(new Usuario("qtal", 40, 2));
            Usuario usu = modelo.leerTodo()[posicionInicio + 2];
            Assert.AreEqual(posicionInicio + 3, modelo.LeerTodos().Count);
            modelo.Eliminar((int)usu.Id);
            Assert.AreEqual(posicionInicio + 2, modelo.LeerTodos().Count);

        }
    }

}