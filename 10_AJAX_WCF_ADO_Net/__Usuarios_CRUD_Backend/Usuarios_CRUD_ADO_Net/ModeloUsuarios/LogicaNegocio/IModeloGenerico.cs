﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ModeloUsuarios
{
    public interface IModeloGenerico<Tipo>
    {
        bool Crear(Tipo nuevoObj);
        IList<Tipo> LeerTodos();
        Tipo LeerUno(int entero);

        Tipo LeerId(int entero);
        bool Eliminar(int entero);

        bool LeerConfirmar(string nombre);

        bool Modificar(Usuario usuario);
    }
}
