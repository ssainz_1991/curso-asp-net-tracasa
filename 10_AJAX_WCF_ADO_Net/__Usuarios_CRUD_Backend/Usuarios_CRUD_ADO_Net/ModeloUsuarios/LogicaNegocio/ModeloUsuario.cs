﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ModeloUsuarios
{
    public delegate bool CrearUsuario(Usuario usuario);
    public delegate bool EliminarUsuario(int entero);
    public delegate IList<Usuario> Leer();
    public delegate Usuario LeerUno(int entero);
    public delegate Usuario LeerId(int entero);
    public delegate bool LeerConfimar(string nombre);
    public delegate bool Modificar(Usuario usu);

    public class ModeloUsuario : IModeloUsuario
    {
        static ModeloUsuario instance = null;
        public CrearUsuario delegadoCrear;
        public EliminarUsuario delegadoEliminar;
        //public LeerUsuario delegateLeer;

        public Leer leerTodo;
        public LeerUno leerUno;
        public LeerId leerId;
        public LeerConfimar leerConfirmar;
        public Modificar modificar;
        public ModeloUsuario() // Contructor de la clase que queremos que solo haya una instancia, se debe poner como private
        {

        }

        public static ModeloUsuario Instancia
        {
            get
            {
                if (instance == null)
                {
                    instance = new ModeloUsuario();
                }

                return instance;
            }
        }

        public bool Crear(Usuario nuevoObj)
        {
            if (nuevoObj != null)
            {
                if (! string.IsNullOrEmpty(nuevoObj.Nombre) && nuevoObj.Nombre != "SIN NOMBRE")
                {   
                    return (bool)delegadoCrear?.Invoke(nuevoObj);
                }                
            }
            return false;
        }

        public bool Eliminar(int entero)
        {
           return delegadoEliminar.Invoke(entero);
        }

        public IList<Usuario> LeerTodos()
        {
            return leerTodo?.Invoke();
        }

        public Usuario LeerUno(int entero)
        {
            return leerUno?.Invoke(entero);
        }

        public Usuario LeerId(int entero)
        {
            return leerId?.Invoke(entero);
        }

        public bool LeerConfirmar(string nombre)
        {
            
            return leerConfirmar.Invoke(nombre);
        }

        public bool Modificar(Usuario usuario)
        {
            return (bool)modificar?.Invoke(usuario);
        }
    }
}
