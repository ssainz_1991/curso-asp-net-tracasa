﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using Usuarios_CRUD_WCF.Controllers;

namespace ModeloUsuarios
{
    //[EnableCors("MyPolicy")]
    [ApiController]
    [Route("api/[controller]")]
    public class UsuariosController : ControllerBase
    {
        IModeloGenerico<Usuario> modeloUsuario;
        private readonly ILogger<UsuariosController> _logger;
        public UsuariosController(ILogger<UsuariosController> logger)
        {
            _logger = logger;
            this.modeloUsuario = ModeloUsuario.Instancia;
        }
        [HttpPost("crear")]      
        public string Crear(Usuario nuevoObj)
        {
            if (modeloUsuario.Crear(nuevoObj) is false)
                throw new Exception("No se ha creado el usuario "
                           + nuevoObj.Nombre);
                //return "Usuario no creado";
            return "Usuario creado correctamente";
        }
        /*public Usuario Crear (Usuario bodyDelJson)
        {
            modeloUsuario.Crear(bodyDelJson);
            return bodyDelJson;
        }*/

        [HttpGet]
        public IList<Usuario> LeerTodos()
        {
            return modeloUsuario.LeerTodos();
        }
        [HttpDelete("eliminar/{entero}")]
        public string Eliminar(int entero)
        {
            if (modeloUsuario.Eliminar(entero) is false)
                throw new Exception($"Ha fallado la eliminación del usuario con ID {entero}");
            return "Usuario eliminado correctamente";
        }
       [HttpGet("leerUno/{entero}")]
        public Usuario LeerId(int entero)
        {
            return modeloUsuario.LeerId(entero);
        }
        [HttpPut("modificar")]
        public string Modificar(Usuario usuario)
        {
            if (modeloUsuario.Modificar(usuario) is false)
                throw new Exception($"No se ha podido modificar el usuario con ID {usuario.Id} y nombre {usuario.Nombre}");
            return "Usuario modificado correctamente"; 
        }
    }
}
