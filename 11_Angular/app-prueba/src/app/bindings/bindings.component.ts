import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bindings',
  template: 
  `<p>¡trozo-web funciona!</p>
  <p>Mostrando valor propiedad con interpolación (variable interpolada)</p>
  <!-- Property bindign: Equivalente al \${expresion} de JS o de C# -->
  {{ (10 + 5) + propiedadClase }}
  <!-- Event bindign: Con los paréntesis vinculamos un evento HTML con un método TS de clase -->
  <input type="button" value="Aumentar" (click)="alPulsarBoton()"/>
  <p>Pulsado {{ contadorComp }} veces</p>
  <!-- Double data-bindign: La leche: los dos anteriores en uno. Se usa con [ ] que también vale
  para el property binding y ( ), a la vez -->
  <span>Cambia el valor: </span><input type="number" [(ngModel)]="contadorComp"/>`
})
export class BindingsComponent implements OnInit {

  propiedadClase: string;
  static contadorEstatico: number = 0;
  contadorComp = 0; // Como declararlo con var de C#

  constructor() {
    this.propiedadClase = "...";
    this.contadorComp = 1;
   }

  ngOnInit(): void {
    BindingsComponent.contadorEstatico ++;
    this.propiedadClase = `ngOnInit es el primer método del ciclo de vidad del componente (ejecutado ${BindingsComponent.contadorEstatico} veces)`;
  }
  alPulsarBoton(): void {
    this.contadorComp ++;
  }

}
