import { Component, Input, OnInit } from '@angular/core';
import { ServicioEjemploService } from '../servicio-ejemplo.service';

@Component({
  selector: 'app-propiedades-input',
  templateUrl: './propiedades-input.component.html',
  styleUrls: ['./propiedades-input.component.css']
})
export class PropiedadesInputComponent implements OnInit {

  @Input()
  valorX: string = "0";
  @Input()
  valorY: string = "0";
  valorZ: string = "0";
  @Input()
  srvActivo: string = "false";

  constructor() { }

  ngOnInit(): void {
  }
}
