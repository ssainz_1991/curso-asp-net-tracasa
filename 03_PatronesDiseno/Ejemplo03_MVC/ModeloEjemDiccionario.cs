﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo03_MVC
{
    public class ModeloEjemDiccionario : IModeloEjemplo
    {
        Dictionary<string, Ejemplo> diccionario;

       public  ModeloEjemDiccionario()
        {
            diccionario = new Dictionary<string, Ejemplo>();
        }
        public Ejemplo Crear(string key, int entero, string str)
        {
            Ejemplo ej = new Ejemplo(entero, str);
            if (diccionario.ContainsKey(key))
            {
                ej = null;
                // diccionario[key] = ej;     // Se puede hacer así y lo sutituye, aunque no se ve porque es el mismo
            }
            else
            {
                diccionario.Add(key, ej);
            } 
            return ej;
        }
        public Ejemplo Crear(int entero, string str)
        {
            string clave = str + "-" + entero;
            return Crear(clave, entero, str);
        }
        public Ejemplo Crear(Ejemplo ejemplo)
        {
            throw new NotImplementedException("No se puede por la clave");
            //TODO: Leer propiedad privada mediante REFLECTION
        }
        public void LeerTodos(out Dictionary<string, Ejemplo> todos)
        {
            todos = diccionario;
        }

        public IList<Ejemplo> LeerTodos()
        {
            IList<Ejemplo> lista = new List<Ejemplo>();
            foreach (var item in diccionario)
            {
                lista.Add(item.Value);
            }
            return lista;
        }
        public Ejemplo LeerUno(string str)
        {
            foreach (var item in diccionario)
            {
                if (item.Value.Str.ToLower().Equals(str.ToLower()))
                {
                    return item.Value;
                }
            }
            return null;
        }
        public Ejemplo LeerUno(int entero)
        {
            foreach (var item in diccionario)
            {
                if (item.Value.EsEntero(entero))
                {
                    return item.Value;
                }
            }
            return null;
        }
        public Ejemplo Modificar(string nombreBusq, int entero, string str)
        {
            foreach (var item in diccionario)
            {
                if (item.Value.Str.ToLower().Equals(nombreBusq.ToLower()))
                {
                    Ejemplo nuevoEjMod = new Ejemplo(entero, str);
                    string clave = item.Key;
                    diccionario[clave] = nuevoEjMod;
                    return nuevoEjMod; // item.Value;
                }
            }
            return null;
        }
        public bool Eliminar(string str)
        {
            foreach (var item in diccionario)
            {
                if (item.Value.Str.Equals(str))
                {
                    diccionario.Remove(item.Key);
                    return true;
                }
            }
            return false;
        }
        public bool Eliminar(int entero)
        {
            foreach (var item in diccionario)
            {
                if (item.Value.EsEntero(entero))
                {
                    diccionario.Remove(item.Key);
                    return true;
                }
            }
            return false;
        }
    }
}
