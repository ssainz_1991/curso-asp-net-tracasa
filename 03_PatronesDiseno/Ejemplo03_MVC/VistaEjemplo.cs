﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo03_MVC
{
    class VistaEjemplo : IVistaEjemplo
    {
        ControladorEjemplo controlador;
        public VistaEjemplo(ControladorEjemplo controlador)
        {
            this.controlador = controlador;
        }
        public void Menu()
        {
            int opcion = 0;

            do
            {
                Console.WriteLine("\n\nQUÉ DESEA HACER?");
                Console.WriteLine("0) Salir");
                Console.WriteLine("1) AltaEjemplo");
                Console.WriteLine("2) MostrarEjemplos");
                Console.WriteLine("3) MostrarUno");
                Console.WriteLine("4) Modificar");
                Console.WriteLine("5) Eliminar");
                string str_op = Console.ReadLine();
                if (int.TryParse(str_op, out opcion))
                {
                    switch (opcion)
                    {

                        case 1:
                            AltaEjemplo();
                            break;
                        case 2:
                            MostrarEjemplos();
                            break;
                        case 3:
                            MostrarUno();
                            break;
                        case 4:
                            ModificarUno();
                            break;
                        case 5:
                            EliminarUno();
                            break;
                        default:
                            Console.Write("Introduzca opcion valida");
                            break;
                    }
                }
                else
                {
                    opcion = -1;
                }
            }
            while (opcion != 0);
        }
        public void AltaEjemplo()
        {
            Console.WriteLine("Alta ejemplo: numero:");
            int entero = int.Parse(Console.ReadLine());
            Console.WriteLine("Alta ejemplo: string:");
            string str = Console.ReadLine();
            Ejemplo ej = this.controlador.AltaEjemplo(entero, str);
            if (ej != null)
            {
                Console.WriteLine("Dado de alta el ejemplo: " + ej.ToString());
            }
            else
            {
                Console.WriteLine("No dado de alta el ejemplo: " + str);
            }
        }
        public void MostrarEjemplos()
        {
            IEnumerable<Ejemplo> todos = controlador.LeerEjemplos();
            foreach (Ejemplo ejemplo in todos)
            {
                Console.WriteLine("Ejemplo " + ejemplo.ToString());
            } 
        }
        public void MostrarUno()
         {
            Console.WriteLine("1 - Busqueda por numero: ");
            Console.WriteLine("2 - Busqueda por nombre: ");
            int opcion;
            string op = Console.ReadLine();
            if (int.TryParse(op, out opcion))
            {
                Ejemplo ejemplo;
                switch (opcion)
                {
                    case 1:
                        UIConsole.PedirNum<int>("Intro entero: ", out int entero);
                        ejemplo = controlador.LeerUno(entero);
                        if (ejemplo != null)
                            Console.WriteLine("Ejemplo " + ejemplo.ToString());
                        else
                            Console.WriteLine("No encontrado por " + entero);
                        break;
                    case 2:
                        UIConsole.PedirTexto("Intro nombre: ", out string nombre);
                        ejemplo = controlador.LeerUno(nombre);
                        if (ejemplo != null)
                            Console.WriteLine("Ejemplo " + ejemplo.ToString());
                        else
                            Console.WriteLine("No encontrado por " + nombre);
                        break;
                }
            }
         }
        public void EliminarUno()
        {            
            UIConsole.PedirTexto("Intro nombre: ", out string nombre);
            bool eliminado = controlador.EliminarUno(nombre);
            if (eliminado != false)
            Console.WriteLine("Ejemplo " + nombre + " eliminado");
            else
            Console.WriteLine("No encontrado por " + nombre); 
        }
        public void ModificarUno()
        {
            UIConsole.PedirTexto("Nombre a modificar: ", out string nombreBusq);
            UIConsole.PedirNum<int>("Nuevo numero: ", out int entero);
            UIConsole.PedirTexto("Nuevo nombre: ", out string str);      
            Ejemplo ejMod = controlador.Modificar(nombreBusq, entero, str);
            if (ejMod != null)
                Console.WriteLine("Ejemplo " + ejMod.ToString());
            else
                Console.WriteLine("No encontrado por " + nombreBusq);
        }
    }

}
