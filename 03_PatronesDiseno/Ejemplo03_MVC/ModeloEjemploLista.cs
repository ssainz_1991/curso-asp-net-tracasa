﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo03_MVC
{
    class ModeloEjemploLista : IModeloEjemplo
    {
        private List<Ejemplo> ejemplos;

        public ModeloEjemploLista()
        {
            ejemplos = new List<Ejemplo>();
        }

        public Ejemplo Crear(Ejemplo ejemplo)
        {
            ejemplos.Add(ejemplo);
            return ejemplo;
        }

        public Ejemplo Crear(int entero, string str)
        {
            return Crear(new Ejemplo(entero, str));
        }
        public List<Ejemplo> LeerTodos()
        {
            return ejemplos;
        }
        IList<Ejemplo> IModeloGenerico<Ejemplo>.LeerTodos()
        {
            return LeerTodos();
        }
        public Ejemplo LeerUno(string str)
        {
            foreach (Ejemplo ejemplo in ejemplos)
            {
                if (ejemplo.Str.ToLower() == str.ToLower())
                    return ejemplo;
            }
            return null;
        }
        public Ejemplo LeerUno(int entero)
        {
            foreach (Ejemplo ejemplo in ejemplos)
            {
                if (ejemplo.EsEntero(entero))
                    return ejemplo;
            }
            return null;
        }
        public Ejemplo Modificar(string nombreBusq, int entero, string str)
        {
            Ejemplo ejemploModificar = LeerUno(nombreBusq);
            if (ejemploModificar != null)
            {
                int posicion = ejemplos.IndexOf(ejemploModificar);
                ejemplos[posicion] = new Ejemplo(entero, str);
                return ejemplos[posicion];
            }
            return null;
        }
        public Ejemplo Modificar2(string nombreBusq, int entero, string str)
        {
            Eliminar(nombreBusq);
            return Crear(new Ejemplo(entero, str));
        }
        public bool Eliminar(string str)
        {
            Ejemplo ejemplo = LeerUno(str);
            if (ejemplo != null)
            {
                return ejemplos.Remove(ejemplo);
            }
            return false;
        }
        public bool Eliminar(int entero)
        {
            Ejemplo ejemplo = LeerUno(entero);
            if (ejemplo != null)
            {
                return ejemplos.Remove(ejemplo);
            }
            return false;
        }
    }
}
