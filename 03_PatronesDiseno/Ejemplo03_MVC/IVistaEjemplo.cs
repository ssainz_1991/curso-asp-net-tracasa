﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo03_MVC
{
    interface IVistaEjemplo
    {
        void AltaEjemplo();
        void MostrarEjemplos();
        void Menu();
        void MostrarUno();
        void EliminarUno();
        void ModificarUno();
    }
}
