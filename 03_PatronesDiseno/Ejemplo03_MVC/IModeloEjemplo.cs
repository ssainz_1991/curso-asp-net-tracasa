﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo03_MVC
{
    interface IModeloEjemplo : IModeloGenerico<Ejemplo>
    {
        public Ejemplo Crear(int entero, string str);
        // IList<Ejemplo> LeerTodos();
        public Ejemplo LeerUno(string str);
        // public Ejemplo LeerUno(int entero);
        Ejemplo Modificar(string nombreBusq, int entero, string str);
        public bool Eliminar(string str);
    }
}
