﻿using System;

namespace Ejemplo03_MVC
{
    class Program
    {
        IModeloEjemplo model1;
        VistaEjemplo ve;
        Program()
        {
            model1 = new ModeloEjemploLista();
            ControladorEjemplo controlador = new ControladorEjemplo(model1);
            // Enganchamos el MVC
            ve = new VistaEjemplo(controlador);
        }
        void ProbarDatos()
        {
            model1.Crear(1, "Uno");
            model1.Crear(2, "Dos");
            model1.Crear(3, "Tres");

            ve.AltaEjemplo();
            ve.MostrarEjemplos();
        }
        static void Main(string[] args)
        {
            Program program = new Program();
            program.ProbarDatos();
            program.ve.Menu();
        }
    }
}
