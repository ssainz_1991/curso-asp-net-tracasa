﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo03_MVC
{
    public class UIConsole
    {
        public static void PedirTexto(string nombreDato, out string varDato)
        {
            Console.WriteLine(nombreDato);
            do
            {
                varDato = Console.ReadLine();
            }
            while (string.IsNullOrEmpty(varDato));
        }
        public static void PedirNum<Tipo>(string nombreDato, out Tipo varNum)
        {
            Console.WriteLine(nombreDato);
            bool esValorOk;
            do
            {
                string str = Console.ReadLine();
                if(typeof(Tipo) == typeof(int))
                {
                    int numero;
                    esValorOk = int.TryParse(str, out numero);
                    varNum = (Tipo)(object)numero;
                }
                else if (typeof(Tipo) == typeof(float))
                {
                    float numero;
                    esValorOk = float.TryParse(str, out numero);
                    varNum = (Tipo)(object)numero;
                }
                else if (typeof(Tipo) == typeof(double))
                {
                    double numero;
                    esValorOk = double.TryParse(str, out numero);
                    varNum = (Tipo)(object)numero;
                }
                else
                {
                    throw new FormatException("ERROR: Tipo de dato no valido");
                }
                if (!esValorOk)
                    Console.WriteLine("Pon un numero, por favor");
            }
            while (!esValorOk);
        }
    }
}
