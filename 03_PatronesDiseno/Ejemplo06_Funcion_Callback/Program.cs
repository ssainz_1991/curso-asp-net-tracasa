﻿using System;

namespace Ejemplo06_Funcion_Callback
{
    // Creamos un nuevo tipo de dato, que indica que es una función (ni calse, ni interfaz...).
    // Donde lo que importa son los tipos de datos que recibe y que devuelve.
    // Es decir, declaramos un delegado.
    delegate float FuncionOperador(float op1, float op2);
    // Ahora podemos crear variables de tipo float funcion(float, float).
    class Program
    {
        static void Main(string[] args)
        {
            /*Console.WriteLine("Invocamos con suma");
            // Al pasar como parametro la función SumarA, no lleva paréntesis, porque NO se llama
            // ahora, sino después.
            VistaCalculadora(Calculadora_A.SumarA);
            Console.WriteLine("Invocamos con multiplicador");
            VistaCalculadora(Calculadora_A.MultiplicarA);
            Console.WriteLine("Invocamos con multiplicador exacto");
            VistaCalculadora(Calculadora_B.MultiplicarB);*/
            VistaOperadora((float x, float y) => x + y, Calculadora_B.MultiplicarB);
        }
        // Esta funcion necesita de una función callback para saber como tiene que calcular.
        static void VistaCalculadora(FuncionOperador operador)
        {
            Console.WriteLine("Operando 1:");
            float x = float.Parse(Console.ReadLine());
            Console.WriteLine("Operando 2:");
            float y = float.Parse(Console.ReadLine());
            float resultado = operador(x, y);
            Console.WriteLine("Resultado: " + resultado);
        }
        static void VistaOperadora(FuncionOperador funSuma, FuncionOperador funMult)
        {
            Console.WriteLine("Introduzca la operacion a realizar:");
            string operacion = Console.ReadLine();
            if (operacion.Contains("+"))
            {
                string[] numeros = operacion.Split("+");
                float resultado = funSuma(float.Parse(numeros[0]), float.Parse(numeros[1]));
                Console.WriteLine("Resultado: " + resultado);
            }
            else if (operacion.Contains("*"))
            {
                string[] numeros = operacion.Split("*");
                float resultado = funMult(float.Parse(numeros[0]), float.Parse(numeros[1]));
                Console.WriteLine("Resultado: " + resultado);
            }
            else
                Console.WriteLine("No te entiendo, humano");
        }
        // Ejercicio 1: Usar VistaCalculadora con una calculadora creada por vosotros, pero exacta.
        // LLamar a la clase CalculadoraB.
        // Ejercicio 2: Crear una VistaOperadora (función estática), que el usuario pueda introducir
        // una única cadena con dos números y el operador. Que calcule usando CalcA ó CalcB u otra diferente.
        // Si lo pone mal que diga "No te entiendo, humano".
        // Ejemplos: 10+22  ó   333*1   ó   2.5*11
    }
}
