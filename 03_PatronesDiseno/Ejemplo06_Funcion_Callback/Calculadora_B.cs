﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo06_Funcion_Callback
{
    public static class Calculadora_B
    {
        public static float SumarB(float b1, float b2)
        {
            return b1 + b2;
        }
        public static float RestarB(float b1, float b2)
        {
            return b1-b2;
        }
        public static float MultiplicarB(float b1, float b2)
        {
            return b1 * b2;
        }
        public static float DividirB(float b1, float b2)
        {
            if (b2 == 0)
                throw new System.DivideByZeroException("Se trato de dividir por 0.");
            return b1 / b2;
        }
    }
}
