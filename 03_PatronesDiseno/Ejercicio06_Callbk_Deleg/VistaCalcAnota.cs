﻿using System;
using System.Collections.Generic;
using System.Text;
using Ejemplo01_Encapsulacion;
using Ejemplo06_Funcion_Callback;

namespace Ejercicio06_Callbk_Deleg
{   //                                                          param1, param2, return
    // public delegate float FuncionArray(float[] numeros, Func<float, float, float> CalcArray);
    public static class VistaCalcAnotaciones
    {
        public static void PedirDatos(Func<float[], Func<float, float, float>, float> CallbkCalcArray)
        {
            int cantidad;
            do
            {
                UIConsole.PedirNum("¿Cuantos operandos habra?", out cantidad);
            }
            while (cantidad < 1);

            float[] numeros = new float[cantidad];
            // float resultado = 0;
            for (int i = 0; i < cantidad; i++)
            {
                UIConsole.PedirNum("Introduzca el siguiente numero", out numeros[i]);
            }

            char operacion;
            Dictionary<char, Func<float, float, float>> operaciones = new Dictionary<char, Func<float, float, float>>();
            operaciones.Add('+', Calculadora_B.SumarB);
            // En lugar de pasar como función callback, una función estática, pública y con nombre,
            // podemos usar funciones sin nombre, o anónimas o también llamadas funciones lambda.
            operaciones.Add('-', (float x, float y) => { return x - y; });
            // Cuando tenemos una sola línea de código con el return, nos podemos ahorra las llaves y el return.
            Func<float, float, float> funMult = (float x, float y) =>  x * y;
            operaciones.Add('*', funMult);
            operaciones.Add('/', (float x, float y) =>
            {
                // Las llaves solo se usan si hay más de una linea de código
                if (y == 0)
                    throw new System.DivideByZeroException("Se trato de dividir por 0.");
                return x / y;
            });
            do
            {
                Console.WriteLine("Introduzca la operacion (+, -, *, /): ");
                operacion = Console.ReadKey().KeyChar;
            }
            while (! operaciones.ContainsKey(operacion));

            Console.WriteLine("\nResultado: " + CallbkCalcArray?.Invoke(numeros, operaciones[operacion]));

            /*switch (operacion)
            {
                case '+':
                    Console.WriteLine("\nResultado: " + CallbkCalcArray?.Invoke(numeros, Calculadora_B.SumarB));
                    break;
                case '-':
                    Console.WriteLine("\nResultado: " + CallbkCalcArray?.Invoke(numeros, Calculadora_B.RestarB));
                    break;
                case '*':
                    Console.WriteLine("\nResultado: " + CallbkCalcArray?.Invoke(numeros, Calculadora_B.MultiplicarB));
                    break;
                case '/':
                    Console.WriteLine("\nResultado: " + CallbkCalcArray?.Invoke(numeros, Calculadora_B.DividirB));
                    break;
            }*/
            
            /*UIConsole.PedirOperacion("¿Que operación deseas realizar?", out string operacion);

            if (operacion.Contains("+"))
            {
                resultado = funSuma(numeros);
            }
            else if (operacion.Contains("-"))
            {
                resultado = funResta(numeros);
            }
            else if (operacion.Contains("*"))
            {
                resultado = funMult(numeros);
            }
            else if (operacion.Contains("/"))
            {
                resultado = funDiv(numeros);
            }
            else
            {
                Console.WriteLine("Operación incorrecta!!!");
            }
            Console.WriteLine("Resultado: " + resultado);*/
        }
    }
}
