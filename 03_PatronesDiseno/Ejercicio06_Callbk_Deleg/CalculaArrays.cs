﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ejercicio06_Callbk_Deleg
{
    public static class CalculaArrays
    {
        public static float CalcularArrays(float[] numeros, Func<float, float, float> OperaArray)
        {
            if (numeros == null)            throw new Exception("El array no puede ser null");
            else if (numeros.Length == 0)   throw new Exception("El array no puede estar vacio");
            else if (OperaArray == null)    throw new Exception("La funcion no puede ser null");

            return numeros.Aggregate(OperaArray);

            /*float resultado = numeros[0];
            for (int i = 1; i < numeros.Length; i++)
            {
                resultado = OperaArray.Invoke(resultado, numeros[i]);
            }
            return resultado;*/
        }
       /* public static float Sumar(float[] numeros)
        {
            if (numeros == null || numeros.Length == 0)
            {
                throw new Exception("El array no puede estar vacio");
            }
            float resultado = numeros[0];
            for (int i = 1; i < numeros.Length; i++)
            {
                resultado += numeros[i];
            }
            return resultado;
        }
        public static float Restar(float[] numeros)
        {
            if (numeros == null || numeros.Length == 0)
            {
                throw new Exception("El array no puede estar vacio");
            }
            float resultado = numeros[0];
            for (int i = 1; i < numeros.Length; i++)
            {
                resultado -= numeros[i];
            }
            return resultado;
        }
        public static float Multiplicar(float[] numeros)
        {
            if (numeros == null || numeros.Length == 0)
            {
                throw new Exception("El array no puede estar vacio");
            }
            float resultado = numeros[0];
            for (int i = 1; i < numeros.Length; i++)
            {
                resultado *= numeros[i];
            }
            return resultado;
        }
        public static float Dividir(float[] numeros)
        {
            if (numeros == null || numeros.Length == 0)
            {
                throw new Exception("El array no puede estar vacio");
            }
            float resultado = numeros[0];
            for (int i = 1; i < numeros.Length; i++)
            {
                resultado /= numeros[i];
            }
            return resultado;
        }*/
    }
}
