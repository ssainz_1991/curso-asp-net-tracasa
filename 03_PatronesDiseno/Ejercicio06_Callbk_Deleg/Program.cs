﻿using System;

// Ejercicio: Crear dos sistemas (clases independientes que no se conocen entre si):
// La primera tendrá 4 funciones para sumar, rest, mult, div, todo con float.
// Estas podrán realizar la operación sobre un array. Ejemplo:
// {9, 7, 5, 3} suma -> 24   resta -> -6     mult -> 945
//      {42, 3, 2}  div -> (42/3)/2 = 7     suma -> 47
//      {3} -> cualquier operador devuelve 3.
// No puede estar vacio el array.
// La segunda clase le pide al usuario cuantos operandos habrá, y los operandos
// uno a uno, la operación, y mostrará el resultado.
// Si alguien quiere, en vez de eso, que resuelva: 3+22+11 ó 7*3*11*2

namespace Ejercicio06_Callbk_Deleg
{   
    class Program
    {
        static void Main(string[] args)
        {
            // VistaCalc.PedirDatos(CalculaArrays.Sumar, CalculaArrays.Restar, CalculaArrays.Multiplicar, CalculaArrays.Dividir);
            VistaCalc.PedirDatos(CalculaArrays.CalcularArrays);
        }
    }
}
