﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo01_Encapsulacion
{
    public class UIConsole
    {
        public static void PedirTexto(string nombreDato, out string varDato)
        {
            Console.WriteLine(nombreDato + "? ");
            do
            {
                varDato = Console.ReadLine();
                // Validación del dato
            }
            while (string.IsNullOrEmpty(varDato));
        }
        public static void PedirOperacion(string nombreDato, out string varDato)
        {
            Console.WriteLine(nombreDato);
            do
            {
                varDato = Console.ReadLine();
            }
            while (string.IsNullOrEmpty(varDato) && varDato != "+" && varDato != "-" && varDato != "*" && varDato != "/");
        }
        // Para pasar un tipo de dato definido por el usuario del método, están los tamplates o métodos genéricos
        public static void PedirNum<Tipo>(string nombreDato, out Tipo varNum)
        {
            Console.WriteLine(nombreDato);
            bool esValorOk;
            do
            {
                string str = Console.ReadLine();
                if(typeof(Tipo) == typeof(int))
                {
                    int numero;
                    esValorOk = int.TryParse(str, out numero);
                    varNum = (Tipo)(object)numero;
                }
                else if (typeof(Tipo) == typeof(float))
                {
                    float numero;
                    esValorOk = float.TryParse(str, out numero);
                    varNum = (Tipo)(object)numero;
                }
                else if (typeof(Tipo) == typeof(double))
                {
                    double numero;
                    esValorOk = double.TryParse(str, out numero);
                    varNum = (Tipo)(object)numero;
                }
                else
                {
                    throw new FormatException("ERROR: Tipo de dato no valido");
                    /*
                    esValorOk = true;
                    varNum = default(Tipo);
                    Console.Error.WriteLine("ERROR: Tipo de dato no valido");
                    */
                    // Lanzamos adrede una excepción
                    // throw new FormatException("ERROR: Tipo de dato no valido");
                }
                if (!esValorOk)
                    Console.WriteLine("Pon un numero, por favor");
            }
            while (!esValorOk);
        }
        /*
        public static void PedirFloat(string nombreDato, out float varNum)
        {
            Console.WriteLine(nombreDato + "? ");
            bool esNumeroOk;
            do
            {
                string str = Console.ReadLine();
                esNumeroOk = float.TryParse(str, out varNum);
                if (!esNumeroOk)
                    Console.WriteLine("Pon un numero, por favor");
            }
            while (!esNumeroOk);
        }
        public static void PedirInt(string nombreDato, out int varNum)
        {
            Console.WriteLine(nombreDato + "? ");
            bool esNumeroOk;
            do
            {
                string str = Console.ReadLine();
                esNumeroOk = int.TryParse(str, out varNum);
                if (!esNumeroOk)
                    Console.WriteLine("Pon un numero, por favor");
            }
            while (!esNumeroOk);
        }
        */
    }
}
