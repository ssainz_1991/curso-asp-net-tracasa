﻿using System;
using System.Collections.Generic;
using System.Text;
using Ejemplo01_Encapsulacion;

namespace Ejercicio06_Callbk_Deleg
{  
    public static class VistaCalc
    {
        public static void PedirDatos(Func<float[], Func<float, float, float>, float> CallbkCalcArray)
        {
            int cantidad;
            do
            {
                UIConsole.PedirNum("¿Cuantos operandos habra?", out cantidad);
            }
            while (cantidad < 1);

            float[] numeros = new float[cantidad];
            for (int i = 0; i < cantidad; i++)
            {
                UIConsole.PedirNum("Introduzca el siguiente numero", out numeros[i]);
            }
            /*
            char operacion;
            do
            {
                Console.WriteLine("Introduzca la operacion (+, -, *, /): ");
                operacion = Console.ReadKey().KeyChar;
            }
            while (operacion != '+' && operacion != '-' && operacion != '*' && operacion != '/');

            switch (operacion)
            {
                case '+':
                    Console.WriteLine("\nResultado: " + CallbkCalcArray?.Invoke(numeros, (float x, float y) => x + y));
                    break;
                case '-':
                    Console.WriteLine("\nResultado: " + CallbkCalcArray?.Invoke(numeros, (float x, float y) => x - y));
                    break;
                case '*':
                    Console.WriteLine("\nResultado: " + CallbkCalcArray?.Invoke(numeros, (float x, float y) => x * y));
                    break;
                case '/':
                    Console.WriteLine("\nResultado: " + CallbkCalcArray?.Invoke(numeros, (float x, float y) =>
                    {
                        if (y == 0)
                            throw new System.DivideByZeroException("Se trato de dividir por 0.");
                        return x / y;
                    }));
                    break;
            }
            */
            UIConsole.PedirOperacion("¿Que operación deseas realizar?", out string operacion);

            switch (operacion)
            {
                case "+":
                    Console.WriteLine("\nResultado: " + CallbkCalcArray?.Invoke(numeros, (float x, float y) => x + y));
                    break;
                case "-":
                    Console.WriteLine("\nResultado: " + CallbkCalcArray?.Invoke(numeros, (float x, float y) => x - y));
                    break;
                case "*":
                    Console.WriteLine("\nResultado: " + CallbkCalcArray?.Invoke(numeros, (float x, float y) => x * y));
                    break;
                case "/":
                    Console.WriteLine("\nResultado: " + CallbkCalcArray?.Invoke(numeros, (float x, float y) =>
                    {
                        if (y == 0)
                            throw new System.DivideByZeroException("Se trato de dividir por 0.");
                        return x / y;
                    }));
                    break;
            }
        }
    }
}
