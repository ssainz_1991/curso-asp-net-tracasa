﻿using System;

namespace Ejemplo04_Observer
{
    class Program
    {
        static void Main(string[] args)
        {
            MovidasObserver();
            Console.WriteLine(">>>>> Funcion terminada");
            // Invocar al Garbage Collector (recolector de basura)
            //      Busca objetos sin referencias, que no haya variables que apunten a ellos
            //      y los elimina, libera su memoria de la RAM.
            System.GC.Collect();
            System.Threading.Thread.Sleep(1000);
        }
        static void MovidasObserver()
        {
            Console.WriteLine("Patron observer: Periodico");
            PeriodicoObservado alDia = new PeriodicoObservado();
            // 1 - Un suscriptor humano
            SuscriptorHumano juan = new SuscriptorHumano("Juan");
            alDia.AddSuscriptor(juan);
            // 2 - Ocurre una noticia
            alDia.NotificarNoticia("Un meteorito roza la luna", DateTime.Now);
            // 3 - Otro suscriptor humano
            SuscriptorHumano pedro = new SuscriptorHumano("Pedro");
            alDia.AddSuscriptor(pedro);
            // 4 - Otro suscriptor maquina, lleva un código int en vez de nombre
            SuscriptorMaquina r2d2 = new SuscriptorMaquina(8582);
            alDia.AddSuscriptor(r2d2);
            // 5 - Ocurre otra noticia
            System.Threading.Thread.Sleep(2000); // Bloquea el Sistema los milisegundos que le indicas por parámetro
            alDia.NotificarNoticia("Hay brotes verdes", DateTime.Now);
            // Noticia del corazon
            alDia.NotificarNoticiaCorazon("Una actriz beso a un fan", DateTime.Now);
            // 6 - El otro humano se desuscribe porque dice mucha mentira
            alDia.DeleteSuscriptor(pedro);
            // 7 - Ocurre la última noticia
            System.Threading.Thread.Sleep(1500);
            alDia.NotificarNoticia("Las ventas del periodico caen en picado", DateTime.Now);
            // 8 - El periodico cierra
            // 9 - Que los suscriptores reciban también la fecha y hora de la noticia y la muestren
            // alDia.CerrarPeriodico();
            Console.WriteLine(">>>>> Terminando funcion");

            // Para que el suscriptor de ciencia se pueda suscribir, simplemente se asigna la función al campo delegado
            alDia.NuevaNotCiencia = NationalGeo;
            // Cuando realmente ocurre la noticia de ciencia
            alDia.NoticiaCiencia("El meteorito existia y era de cryptonita");
        }
        // Aquí las variables locales (primitivas, estructs o referencias a objetos) se destruyen,
        // pero no los objetos en sí, esos quedan vivos en alguna parte, pero sin que nadie los recuerde.

        // Este suscriptor sólo es una función
        static void NationalGeo(string noticiaDePseudociencia)
        {
            Console.WriteLine(">>>>> A saber: " + noticiaDePseudociencia);
        }
    }
}
