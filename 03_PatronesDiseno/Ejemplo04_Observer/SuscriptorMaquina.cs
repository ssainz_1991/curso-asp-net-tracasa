﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo04_Observer
{
    class SuscriptorMaquina : ISuscriptorObservador
        {
        int codigo;

        public SuscriptorMaquina(int maquina)
        {
            this.codigo = maquina;
        }

        public void ActualizarNoticiaCorazon(string noticia, DateTime fecha)
        {
            Console.WriteLine("Pasando del tema...");
        }

        public void ActualizarNotificacionNoticia(string noticia, DateTime fecha)
        {
            Console.Beep();
            Console.WriteLine("¡Nuevas noticias, maquina " + codigo + "!");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine(noticia + " ==> " + fecha.ToString("f"));
            Console.ForegroundColor = ConsoleColor.White;
            System.IO.File.AppendAllText("../../../ficha_maquina.txt", "\n" + fecha.ToString("F") + " - " + noticia);
        }
        public override string ToString()
        {
            return this.codigo.ToString();
        }
    }
}
