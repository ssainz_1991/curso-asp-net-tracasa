﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo04_Observer
{
    delegate void NoticiaCiencia(string not);
    class PeriodicoObservado
    {
        IList<ISuscriptorObservador> listaSuscriptores;
        public NoticiaCiencia NuevaNotCiencia;
        public PeriodicoObservado()
        {
            listaSuscriptores = new List<ISuscriptorObservador>();
        }
        public void NoticiaCiencia(string laNoticia)
        {
            if (NuevaNotCiencia != null)
            {
                NuevaNotCiencia("CIENCIA: " + laNoticia);
            }
        }
        public void NotificarNoticia(string titular, DateTime fecha)
        {
            Console.WriteLine("¡Extra, extra!");
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine(fecha.ToString() + " -> " + titular);
            Console.ForegroundColor = ConsoleColor.White;
            foreach (var suscriptor in listaSuscriptores)
            {
                suscriptor.ActualizarNotificacionNoticia(titular, fecha);
            }
        }
        public void NotificarNoticiaCorazon(string titular, DateTime fecha)
        {
            Console.WriteLine("¡Extra, extra!");
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine(fecha.ToString() + " -> " + titular);
            Console.ForegroundColor = ConsoleColor.White;
            foreach (var suscriptor in listaSuscriptores)
            {
                suscriptor.ActualizarNoticiaCorazon(titular, fecha);
            }
        }
        public void AddSuscriptor(ISuscriptorObservador observador)
        {
            listaSuscriptores.Add(observador);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Se ha añadido el nuevo suscriptor " + observador.ToString() + " al periodico.");
            Console.ForegroundColor = ConsoleColor.White;
        }
        public void DeleteSuscriptor(ISuscriptorObservador observador)
        {
            listaSuscriptores.Remove(observador);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Se ha eliminado el suscriptor " + observador.ToString() + " del periodico.");
            Console.ForegroundColor = ConsoleColor.White;
        }
        public void CerrarPeriodico()
        {
            listaSuscriptores.Clear();
            Console.WriteLine("\n\n¡¡¡El periodico ha cerrado...pulse cualquier tecla para salir!!!");
            Console.ReadKey();
            Console.Clear();
        }
        ~PeriodicoObservado()
        {
            NotificarNoticia("¡¡¡El periodico ha cerrado!!!", new DateTime(2021, 08, 21, 11, 00, 00));
            listaSuscriptores.Clear();
        }
    }
}
