﻿using OtroNameSpace;
using System;

namespace Ejercicio08_Strategy_Lambdas
{
    // MainApp Test para aplicacion
    class MainApp
    {
        static void Main()
        {
            StrategyObject estrategiaA = ConcretesStrategies.NewConcreteStrategyA("Llamar a");
            estrategiaA.Execute();

            StrategyObject estrategiaB = ConcretesStrategies.NewConcreteStrategyB();
            estrategiaB.Execute();

            StrategyObject estrategiaC = ConcretesStrategies.NewConcreteStrategyC("Mostar Context C");
            estrategiaC.Execute();
        }
    }
    static class ConcretesStrategies
    {
        // Siempre podéis hacer delegates por aquí fuera
        // Estás funciones tienen que funcionar como antes los constructores
        public static StrategyObject NewConcreteStrategyA(string mensaje)
        {
            // Dinámicamente le damos la funcionalidad que antes haciamos por herencia
            // Tanto public override void Execute()
            // como public override void MostrarNombre()
            // strObj.Execute = () => { };
            // strObj.Execute = Execute;
            // MostrarNombre puede ser Func<...> SequencePosition devuelve algo
            // ó Action<...>   SequencePosition son func tipo void, no devuelve nada
            StrategyObject strObj = new StrategyObject(mensaje);
            strObj.Type = "ConcreteStrategyA";
            strObj.Execute = () =>
            {
                strObj.RepetirChar('-', 30);
                strObj.MostrarNombre();
            };
            // Clousure: encapsular/proteger/guardar/almacenar una variable local
            // para que posteriormente pueda usarse en una funcón lambda.
            bool boolClausurado = true;
            // Aquí estamos "clausurando" la antigua función mostrarNombre, para usarse 
            // dentro de la nueva función lambda, mostrarNombre
            Action base_MostrarNombre = strObj.MostrarNombre;
            strObj.MostrarNombre = () =>
            {
                strObj.RepetirChar('_', 30);
                base_MostrarNombre();
                strObj.RepetirChar('_', 30);
                // Simplemente por usarse en una función interna a otro bloque de 
                // código, la variable persiste en el tiempo, lo que dure la función.
                Console.WriteLine("Usamos la var clausurada: " + boolClausurado);
            };
            return strObj;
        }   // En teoría, al finalizar el bloque, todas las var locales se destruyen.
        public static StrategyObject NewConcreteStrategyB()
        {
            StrategyObject strObj = new StrategyObject("Llamar a");
            strObj.Type = "ConcreteStrategyB";
            strObj.Execute = () =>
            {
                strObj.RepetirChar('\n', 3);
                strObj.MostrarNombre();
            };
            return strObj;
        }
        public static StrategyObject NewConcreteStrategyC(string nombre)
        {
            StrategyObject strObj = new StrategyObject(nombre);
            strObj.Type = "ConcreteStrategyC";
            strObj.GetNombre = () => nombre; // equivale a  { return nombre;}
            //                    // Por usar nombre en una lambda, hemos clausurado el parámetro
            strObj.Execute = () =>
            {
                strObj.RepetirChar('*', 20);
                Console.WriteLine("Called ConcreteStrategyC.Execute()");
                Console.WriteLine(strObj.GetNombre());
            };
            return strObj;
        }
    }
    /*
     *  class ConcreteStrategyA : StrategyBase
    {
        public ConcreteStrategyA(string mensaje) : base(mensaje) { }
        public override void Execute()
        {
            RepetirChar('-', 30);
            MostrarNombre();
        }
        public override void MostrarNombre()
        {
            RepetirChar('_', 30);
            base.MostrarNombre();
            RepetirChar('_', 30);
        }
    }

    class ConcreteStrategyB : StrategyBase
    {
        public ConcreteStrategyB() : base("Llamar a") { }
        public override void Execute()
        {
            RepetirChar('\n', 3);
            MostrarNombre();
        }
    }

    class ConcreteStrategyC : StrategyBase
    {
        public ConcreteStrategyC(string nombre) : base("") 
        {
            this.nombre = nombre;
        }
        public override void Execute()
        {
            RepetirChar('*', 20);
            MostrarNombre();
        }
    }
*/
}
    
