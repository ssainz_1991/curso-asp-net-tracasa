﻿using System;

namespace Ejemplo05_Lambdas
{
    // Vamos a crear un nuevo tipo de dato, que en vez de almacenar información, 
    // almacene una función con determinada estructura ó firma.
    // La estructura es como la interfaz de una función, pero sin importar el nombre:
    // los tipos de datos que recibe y el tipo de dato que devuelve.
    // En C eran punteros a funciones, en Java hasta Java8 eran interfaces con una sola función,
    // en JS funciones flecha, en C# son los delegados
    delegate void FuncionQueRecibeInt(int param);

    class Program
    {
        static void Main(string[] args)
        {
            // LAMBDAS
            // Funciones estáticas:
            FuncionEstatica(5);
            OtraEstatica(7);
            // Variables de tipo funcion (delegados)
            FuncionQueRecibeInt funRecint;
            funRecint = FuncionEstatica;
            funRecint(150);
            // funRecint = null;    // Provocaría una excepción
            funRecint = OtraEstatica;
            funRecint(100);
            Console.WriteLine("\n\nOtra libreria, otro modulo, otra funcion, hace:");
            OtroSistema(OtraEstatica);
            OtroSistema(funRecint);
            OtroSistema(FuncionEstatica);
        }
        static void OtroSistema(FuncionQueRecibeInt funExt)
        {
            // Queremos recibir una función como parametro:
            // LlamarFuncionExterna por ejemplo, que reciba un int
            int valor = 3;
            Console.WriteLine("Otro sistema hace sus cosas");
            System.Threading.Thread.Sleep(1000);
            Console.WriteLine("Tardar su tiempo");
            Console.WriteLine("Y llamar a la funcionalidad externa");
            funExt(3);
        }
        static void FuncionEstatica(int x)
        {
            Console.WriteLine("No necesito un objeto para ser llamada");
            Console.WriteLine("Param: " + x);
        }
        static void OtraEstatica(int y)
        {
            Console.WriteLine(y + " - Otra Estatica");
        }
    }
}
