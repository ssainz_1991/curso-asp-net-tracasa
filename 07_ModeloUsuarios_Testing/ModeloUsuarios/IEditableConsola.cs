﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ModeloUsuarios
{
    public interface IEditableConsola
    {
        void PedirDatos();
        void MostrarDatos();
    }
}
