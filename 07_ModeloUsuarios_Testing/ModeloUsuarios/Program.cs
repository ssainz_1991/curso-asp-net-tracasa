﻿using System;

namespace ModeloUsuarios
{
    class Program
    {
        static void Main(string[] args)
        {
            ModuloPersistencia moduloPersistencia = new ModuloPersistencia(Entorno.Produccion);
            IModeloGenerico<Usuario> modelo = ModeloUsuario.Instancia;
            Controlador controlador = new Controlador(modelo);
            Vista vista = new Vista(controlador);
            vista.Menu();
        }
     }
}
