﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Text.Json;

using System.Text.Json.Serialization;

namespace ModeloUsuarios
{
    public class ModuloPersistenciaADO
    {
        string CONEX_BD;
        static Entorno entorno = Entorno.Desarrollo;
        ModeloUsuario modelo;

        List<Usuario> listaUsuarios = new List<Usuario>();


        public ModuloPersistenciaADO(Entorno entorno)
        {
            ModuloPersistenciaADO.entorno = entorno;

            if (entorno == Entorno.Produccion)
            {
                CONEX_BD = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\BD\\bd_usu.mdf;Integrated Security=True;Connect Timeout=30";
            }
            else
            {
                CONEX_BD = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\BD\\bd_usu_test.mdf;Integrated Security=True;Connect Timeout=30";
            }

            ModeloUsuario.Instancia.delegadoCrear = Crear;
            ModeloUsuario.Instancia.delegadoEliminar = Eliminar;
            ModeloUsuario.Instancia.leerTodo = Leer;
            ModeloUsuario.Instancia.leerUno = LeerUno;
            ModeloUsuario.Instancia.leerId = LeerId;
            ModeloUsuario.Instancia.leerConfirmar = LeerConfirmar;
            ModeloUsuario.Instancia.modificar = Modificar;
            listaUsuarios = Leer();
        }
        public bool Crear(Usuario usuario)
        {
            try
            {
                SqlConnection conexion;

                using (conexion = new SqlConnection(CONEX_BD))
                {
                    int marcador_activo = 0;
                    if ((bool)usuario.Activo)
                        marcador_activo = 1;

                    conexion.Open();

                    /*comando.CommandText = "INSERT INTO Usuario (nombre, email, altura, edad, activo) VALUES (@nombre, @email, @altura, @edad, @activo)";
                    comando.Parameters.AddWithValue("@nombre", usuario.Nombre);
                    comando.Parameters.AddWithValue("@email", usuario.Nombre.ToLower().Replace(' ', '_') + "@email.es");
                    comando.Parameters.AddWithValue("@altura", usuario.Altura.ToString().Replace(',', '.'));
                    comando.Parameters.AddWithValue("@edad", usuario.Edad);
                    comando.Parameters.AddWithValue("@activo", usuario.Activo);*/

                    SqlCommand comando = conexion.CreateCommand();
                    comando.CommandText = "INSERT INTO Usuario (nombre, email, altura, edad, activo) VALUES ('"
                        + usuario.Nombre + "', '"
                        + usuario.Nombre.ToLower().Replace(' ', '_')
                        + "@email.es', "
                        + usuario.Altura.ToString().Replace(',', '.') + ", "
                        + usuario.Edad + "," + marcador_activo +")";
                    int filasAfectadas = comando.ExecuteNonQuery();
                    if (filasAfectadas != 1)
                        throw new Exception("No ha hecho insert bbdd "
                            + comando.CommandText);
                    else
                        listaUsuarios.Add(usuario);
                }
                Console.WriteLine("Introducido en bbdd: " + usuario.Nombre);
                Leer();
                return true;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Error añadiendo en bbdd: " + ex.Message);
                return false;
            }
        }
        public bool Eliminar(int IdAux)
        {
            int indice = 0;
            foreach (Usuario usu in listaUsuarios)
            {
                if (usu.Id == IdAux)
                    break;
                indice += 1;

            }
            try
            {
                SqlConnection conexion;

                using (conexion = new SqlConnection(CONEX_BD))
                {
                    conexion.Open();

                    SqlCommand comando = conexion.CreateCommand();
                    //EL ID 14 NO DEBERIA ESTAR LLEGANDO AQUÍ
                    comando.CommandText = "DELETE FROM Usuario WHERE Id = " + IdAux;
                    int filasAfectadas = comando.ExecuteNonQuery();
                    if (filasAfectadas != 1)
                        throw new Exception("No se ha borrado en bbdd "
                            + comando.CommandText);
                }

                listaUsuarios.RemoveAt(indice);
                Console.WriteLine("Leidos de bbdd: " + listaUsuarios.Count);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Error leyendo bbdd: " + ex.Message);
                return false;
            }
            return true;
        }
        public List<Usuario> Leer()
        {
            listaUsuarios = new List<Usuario>();
            try
            {
                SqlConnection conexion;

                using (conexion = new SqlConnection(CONEX_BD))
                {
                    conexion.Open();

                    Usuario usuario;
                    SqlCommand comando = conexion.CreateCommand();
                    comando.CommandText = "SELECT Id, nombre, altura, edad, activo FROM Usuario";
                    SqlDataReader lectorDR = comando.ExecuteReader();

                    while (lectorDR.Read())
                    {
                        usuario = new Usuario();
                        usuario.Id = (int?)lectorDR["Id"];
                        usuario.Nombre = lectorDR[1].ToString();
                        usuario.Altura = (float)(double)lectorDR["altura"];
                        usuario.Edad = lectorDR.GetByte(3);
                        usuario.Activo = (bool)lectorDR[4];

                        listaUsuarios.Add(usuario);
                    }
                }   // conexion.Close();
                Console.WriteLine("Leidos de bbdd: " + listaUsuarios.Count);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Error leyendo bbdd: " + ex.Message);
            }
            return listaUsuarios;
        }

        public int? IdUltimo()
        {
            return listaUsuarios[listaUsuarios.Count - 1].Id;
        }


        public Usuario LeerUno(int entero)
        {
            if (listaUsuarios.Count > entero)
            {
                return listaUsuarios[entero];
            }
            else
            {
                return null;
            }
        }
        public Usuario LeerId(int entero )
        {
            foreach (Usuario usu in listaUsuarios)
            {
                if (usu.Id == entero)
                    return usu;
            }
            return null;
        }

        public bool LeerConfirmar(string nombre)
        {
            int posicion = 0;
            for (int i = 0; i < listaUsuarios.Count; i++)
            {
                if (listaUsuarios[i].GetNombre() == nombre)
                {
                    posicion = i;
                    return true;
                }
            }
            return false;
        }

        public bool Modificar(Usuario usu)
        {
            for (int i = 0; i < listaUsuarios.Count; i++)
            {
                if (listaUsuarios[i].Id == usu.Id)
                {
                    listaUsuarios[i] = usu;
                    try
                    {
                        SqlConnection conexion;

                        using (conexion = new SqlConnection(CONEX_BD))
                        {

                            conexion.Open();
                            int marcador_activo = 0;
                            if ((bool)usu.Activo)
                                marcador_activo = 1;

                            SqlCommand comando = conexion.CreateCommand();
                            comando.CommandText = "UPDATE Usuario SET Nombre = '" + usu.Nombre + "' , Altura = " + usu.Altura.ToString().Replace(',', '.') + ", Edad = " + usu.Edad +", activo= "+ marcador_activo+" WHERE Id = " + usu.Id;
                            Console.WriteLine(comando.CommandText);
                            int filasAfectadas = comando.ExecuteNonQuery();
                            if (filasAfectadas != 1)
                                throw new Exception("No se ha actualizado en bbdd "
                                    + comando.CommandText);
                        }
                        Console.WriteLine("Leidos de bbdd: " + listaUsuarios.Count);
                        return true;
                        
                    }
                    catch (Exception ex)
                    {
                        Console.Error.WriteLine("Error leyendo bbdd: " + ex.Message);
                        return false;
                    }
                }
            }
            return false;

        }
    }
}
