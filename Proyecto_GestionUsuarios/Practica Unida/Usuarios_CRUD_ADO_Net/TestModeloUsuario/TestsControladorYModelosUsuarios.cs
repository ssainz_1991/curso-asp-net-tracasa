using NUnit.Framework;
using ModeloUsuarios;
using System;
using System.Reflection;

namespace TestModeloUsuario
{
    public class TestsControladorYModelosUsuarios
    {
        ModeloUsuario modelo;
        ModuloPersistencia moduloPersistencia;
        Usuario usu1;

        void LimpiarJSON()
        {
            // string path = "C:/Users/pmpcurso1/Desktop/hola.json";
            System.IO.File.Delete(ModuloPersistencia.Fichero);
            // System.IO.File.WriteAllText(ModuloPersistencia.Fichero, "[]");
        }
        void ResetearModeloYCtrl_ConReflection ()
        {
            // FieldInfo[] camposModelo = ModeloUsuario.Instancia.GetType().GetFields(...);
            FieldInfo[] camposModelo = typeof(ModeloUsuario).GetFields(
                BindingFlags.NonPublic | BindingFlags.Static);
            foreach (FieldInfo campo in camposModelo)
            {
                if (campo.Name.Equals("instance"))
                {
                    ModeloUsuario.Instancia.ToString();
                    Console.WriteLine("Encontrado instance Modelo y eliminado");
                    campo.SetValue(null, null);
                    ModeloUsuario.Instancia.ToString();
                }
            }
            moduloPersistencia = new ModuloPersistencia(Entorno.Desarrollo);
            modelo = ModeloUsuario.Instancia;
            //  type.GetField("instance", BindingFlags.NonPublic | BindingFlags.Static).SetValue(null, null);
        }
        void EliminarTodosUsuarios()
        {
            // var lista = ModeloUsuario.Instancia.LeerTodos();

            while (ModeloUsuario.Instancia.LeerTodos().Count > 0)
            {
                ModeloUsuario.Instancia.Eliminar(0);
            }
            /*while (ModeloUsuario.Instancia.LeerTodos().Count > 0)
            {
                var primero = ModeloUsuario.Instancia.LeerTodos()[0];
                ModeloUsuario.Instancia.Eliminar(primero.Nombre);
            }*/
        }

        [SetUp]
        public void Setup()
        {
            moduloPersistencia = new ModuloPersistencia(Entorno.Desarrollo);
            modelo = ModeloUsuario.Instancia;
        }
        [TearDown]
        public void AlTerminarTest()
        {
            // ResetearModeloYCtrl_ConReflection();
            EliminarTodosUsuarios();
            if ((new Random()).Next(3) == 0)
                LimpiarJSON();
        }
        
        void CrearUsuariosValidos()
        {
            usu1 = new Usuario("hola", 27, 1.5f);
            modelo.Crear(usu1);
            modelo.Crear(new Usuario("adios", 33, 2));
            modelo.Crear(new Usuario("qtal", 45, 2));
        }
           
        [Test]
        public void TestCrearValidos()
        {
            CrearUsuariosValidos();
            modelo.Crear(new Usuario("adios2", -3, 2.2f));
            modelo.Crear(new Usuario("qtal2", 400, 27));
            Assert.AreEqual(1, modelo.LeerUno(3).Edad);
            Assert.AreEqual(1.0f, modelo.LeerUno(4).Altura);
            Assert.AreEqual("hola", modelo.LeerUno(0).Nombre);
            Assert.Null(modelo.LeerUno(6));
            Assert.AreEqual(modelo.LeerTodos().Count, 5, "Mal creados los usuarios");
            Assert.IsTrue(modelo.LeerUno(0).Equals(new Usuario("hola", 27, 1.5f)));
        }

        [Test]
        public void TestCrearInvalidos()
        {
            modelo.Crear(new Usuario(null, 400, 27));
            modelo.Crear(null);
            Assert.AreEqual(modelo.LeerTodos().Count, 0, "Mal creados los usuarios");
        }

            [Test]
        public void TestModificar()
        {
            CrearUsuariosValidos();
            Usuario usu1mod = new Usuario("hola", -3, 7);//Te lo cambia aqu� directamente
            modelo.Modificar(usu1mod);
            Assert.AreNotEqual(modelo.LeerUno(0), usu1);
            Assert.AreEqual(modelo.LeerUno(0), usu1mod);
            Usuario usuNoExiste = new Usuario("NoExisto",88 ,88 );
            modelo.Modificar(usuNoExiste);
            for(int i=0;i< modelo.LeerTodos().Count; i++)
            {
                Assert.AreNotEqual(modelo.LeerUno(i), usuNoExiste);
            }
        }
       

        [Test]
        public void TestEliminar()
        {
            Usuario usu1 = new Usuario("hola", 1, 1);
            modelo.Crear(usu1);
            modelo.Crear(new Usuario("adios", 43, 2));
            modelo.Crear(new Usuario("qtal", 40, 2));
            Usuario usu = modelo.LeerTodos()[1];
            Assert.AreEqual(true, modelo.Eliminar(1));
            foreach(Usuario usuario in modelo.LeerTodos())
            {
                Assert.AreEqual(false, usuario == usu);
            }
            Assert.AreEqual(2, modelo.LeerTodos().Count);
            Assert.AreEqual(false, modelo.Eliminar(5));
            Assert.AreEqual(2, modelo.LeerTodos().Count);
        }

        [Test]
        public void TestLeerUno()
        {
            Usuario usu1 = new Usuario("hola", 1, 1);
            modelo.Crear(usu1);
            modelo.Crear(new Usuario("adios", 43, 2));
            modelo.Crear(new Usuario("qtal", 40, 2));
            Assert.Null(modelo.LeerUno(4));
            Assert.NotNull(modelo.LeerUno(1));
            Assert.IsInstanceOf<Usuario>(modelo.LeerUno(1));
       
        }
        
        [Test]
        public void TestLeerTodos()
        {
            Assert.AreEqual(0, modelo.LeerTodos().Count);
            modelo.Crear(new Usuario("adios", 43, 2));
            modelo.Crear(new Usuario("qtal", 40, 2));
            Assert.AreEqual(2, modelo.LeerTodos().Count);
            modelo.Eliminar(0);
            Assert.AreEqual(1, modelo.LeerTodos().Count);

        }
        [Test]
        public void ProbarGuardadoFichero()
        {
            TestCrearValidos();

            ResetearModeloYCtrl_ConReflection();

            Assert.IsTrue(modelo.LeerUno(4).Equals(new Usuario("qtal2", 400, 27)));
        }
    }

}