using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Diagnostics;
using System.IO;

namespace TestSelenium
{
    public class Tests
    {
        #region carga Y m�todos auxiliares
        IWebDriver driver;
        Process _process;
        string ruta_para_driver;
        int cuentaOriginal;

        private void StartServer()
        {
            string projectName = "UsuariosWeb_APIRest";
            string applicationPath = "C:\\Users\\pmpcurso1\\Desktop\\CursoTracasa\\10_AJAX_WCF_ADO_Net\\UsuariosWeb_APIRest";
            _process = new Process
            {
                StartInfo =
                    {
                    FileName = @"dotnet.exe",
                    //Arguments = $@"run {applicationPath}\{projectName}.csproj"
                    Arguments = "run --project C:\\Users\\pmpcurso1\\Desktop\\Curso_Tracasa_SergioSainz\\practica-curso-net.git\\Practica Unida\\UsuariosCRUD\\UsuariosWeb_APIRest\\UsuariosWeb_APIRest.csproj"
                    }
            };
            _process.Start();
        }
        [OneTimeSetUp]
        public void InicializarClaseTest()
        {
            string fichFirefox = "../../../../FirefoxPortable/App/Firefox64/firefox.exe";
            if (!File.Exists(fichFirefox))
            {
                string instalador = "../../../../FirefoxPortable_92.0_English.paf.exe";
                if (File.Exists(instalador))
                {
                    System.Diagnostics.Process.Start(instalador);
                }
            }
            if (File.Exists(fichFirefox))
            {
                FirefoxDriverService geckoService = FirefoxDriverService.CreateDefaultService("./");
                geckoService.Host = "::1";

                FirefoxOptions firefoxOptions = new FirefoxOptions();
                firefoxOptions.BrowserExecutableLocation = fichFirefox;
                firefoxOptions.AcceptInsecureCertificates = true;

                driver = new FirefoxDriver(geckoService, firefoxOptions);
            }

            //Assert.IsTrue(File.Exists("../../../../../Vista.html"), "El Fichero html de la vista no se ha encontrado");
            Assert.IsTrue(File.Exists("C:\\Users\\pmpcurso1\\Desktop\\Curso_Tracasa_SergioSainz\\practica-curso-net.git\\Practica Unida\\UsuariosCRUD\\UsuariosWeb_APIRest\\wwwroot\\Vista.html"), "El Fichero html de la vista no se ha encontrado");
           // ruta_para_driver = "file:///" + System.IO.Path.GetFullPath("../../../../../Vista.html");
            ruta_para_driver = ("http://localhost:1349/Vista.html");
            driver.Navigate().GoToUrl(ruta_para_driver);
            IWebElement tabla = driver.FindElement(By.Id("tabla-usu"));
            cuentaOriginal = tabla.FindElements(By.TagName("tr")).Count;
        }

        public void modificaEste(int indice, string nombre, int edad, float altura, bool Activo)
        {
            //MODIFICAR
            IWebElement btnMod = driver.FindElement(By.XPath($"html/body/table/tbody/tr[{indice}]/td[5]/div/button[2]"));
            btnMod.Click();

            IWebElement elemNombre = driver.FindElement(By.Id("nombre"));
            elemNombre.Clear();
            elemNombre.SendKeys(nombre);

            IWebElement elemEdad = driver.FindElement(By.Id("edad"));
            elemEdad.Clear();
            elemEdad.SendKeys(edad.ToString());

            IWebElement elemAltura = driver.FindElement(By.Id("altura"));
            elemAltura.Clear();
            elemAltura.SendKeys(altura.ToString());

            IWebElement elemActivo = driver.FindElement(By.Id("activo"));
            if (elemActivo.GetProperty("checked").ToLower() != Activo.ToString().ToLower())
                elemActivo.Click();

            IWebElement btnGuardarCambios = driver.FindElement(By.Id("btn-guardar"));
            btnGuardarCambios.Click();
        }
        public void crearUsuario(string nombre, int edad, float altura, bool activo)
        {
            IWebElement elementoNombre = driver.FindElement(By.Id("nombre"));
            elementoNombre.Clear();
            elementoNombre.SendKeys(nombre);

            IWebElement elementoEdad = driver.FindElement(By.Id("edad"));
            elementoEdad.Clear();
            elementoEdad.SendKeys(edad.ToString());

            IWebElement elementoAltura = driver.FindElement(By.Id("altura"));
            elementoAltura.Clear();
            elementoAltura.SendKeys(altura.ToString());

            IWebElement elementoActivo = driver.FindElement(By.Id("activo"));
            if (elementoActivo.GetProperty("checked").ToLower() != activo.ToString().ToLower())
                elementoActivo.Click();

            IWebElement elementoButton = driver.FindElement(By.Id("btn-guardar"));
            elementoButton.Click();
        }

        private void crearVariosUsuarios()
        {
            crearUsuario("primero", 18, 120, true);
            crearUsuario("Eustaquio jos� de los monteros", 40, 180, false);
        }

        public void EliminarUsuario(int indice)
        {
            IWebElement elementoButton = driver.FindElement(By.XPath($"html/body/table/tbody/tr[{indice}]/td[5]/div/button[1]"));
            elementoButton.Click();
        }

        public void EliminarUsuariosTests()
        {
            IWebElement tabla = driver.FindElement(By.Id("tabla-usu"));
            int CountRows = tabla.FindElements(By.TagName("tr")).Count;

            while (CountRows > cuentaOriginal)
            {
                EliminarUsuario(CountRows - 1);
                CountRows -= 1;
            }
        }

        public void Wait(int seg, int timeOut = 60)
        {
            var wait = new WebDriverWait(driver, new TimeSpan(0, 0, 1, timeOut));
            var delay = new TimeSpan(0, 0, 0, seg);
            var timeIni = DateTime.Now;
            wait.Until(driver => (DateTime.Now - timeIni) > delay);
        }

        public void Recargar()
        {
            Wait(1);
            driver.Navigate().Refresh();
            Wait(1);
        }
        #endregion

        #region TESTS
        [Test(Author = "Sergio S", Description = "Crear Usuarios Invalidos")]
        public void TestCrearUsuariosInvalidos()
        {
            crearVariosUsuarios();

            IWebElement tablaLocal = driver.FindElement(By.Id("tabla-usu"));
            int cuentaLocal = tablaLocal.FindElements(By.TagName("tr")).Count;

            IWebElement celdaNombre = driver.FindElement(By.Id("nombre"));
            IWebElement celdaEdad = driver.FindElement(By.Id("edad"));
            IWebElement celdaAltura = driver.FindElement(By.Id("altura"));
            IWebElement botonA�adir = driver.FindElement(By.Id("btn-guardar"));

            celdaNombre.Clear();
            string nombre = driver.FindElement(By.Id("nombre")).GetAttribute("value");
            Assert.IsEmpty(nombre, "El nombre no est� vacio");
            celdaEdad.SendKeys("AA");
            string edad = driver.FindElement(By.Id("edad")).GetAttribute("value");
            Assert.IsFalse(int.TryParse(edad, out int edadSalida), "La edad es un numero");
            celdaAltura.SendKeys("BB");
            string altura = driver.FindElement(By.Id("altura")).GetAttribute("value");
            Assert.IsFalse(float.TryParse(altura, out float alturaSalida), "La altura es un float");
            botonA�adir.Click();

            var fila = driver.FindElements(By.XPath($"/html/body/table/tbody/tr[{cuentaLocal + 1}]"));
            Assert.IsEmpty(fila, "La fila sigue teniendo valores");
            EliminarUsuariosTests();
        }

        [Test(Author = "Sergio S", Description = "Modificar Usuarios Invalidos")]
        public void TestModificarUsuariosInvalidos()
        {
            crearVariosUsuarios();
            Recargar();

            IWebElement botonGuardarModificado = driver.FindElement(By.Id("btn-guardar"));
            string filaOrig = driver.FindElements(By.XPath("/html/body/table/tbody/tr"))[0].Text;

            IWebElement botonModificar = driver.FindElement(By.XPath("/html/body/table/tbody/tr/td[5]/div/button[2]"));
            botonModificar.Click();

            IWebElement celdaNombre = driver.FindElement(By.Id("nombre"));
            celdaNombre.Clear();
            string nombre = driver.FindElement(By.Id("nombre")).GetAttribute("value");
            Assert.IsEmpty(nombre, "El nombre no est� vacio");
            botonGuardarModificado.Click();
            var filaMod = driver.FindElements(By.XPath("/html/body/table/tbody/tr"));

            Assert.AreEqual(filaOrig, filaMod[0].Text, "La tabla ha cambiado");
            IWebElement celdaEdad = driver.FindElement(By.Id("edad"));
            celdaEdad.SendKeys("AA"); //no importa si limpiamos o no el input. 11AA es lo mismo que AA
            string edad = driver.FindElement(By.Id("edad")).GetAttribute("value");
            Assert.IsFalse(int.TryParse(edad, out int edadSalida), "La edad es un numero");
            botonGuardarModificado.Click();
            filaMod = driver.FindElements(By.XPath("/html/body/table/tbody/tr"));
            Assert.AreEqual(filaOrig, filaMod[0].Text, "La tabla ha cambiado");

            IWebElement celdaAltura = driver.FindElement(By.Id("altura"));
            celdaAltura.SendKeys("BB"); //no importa si limpiamos o no el input. 00BB es lo mismo que BB
            string altura = driver.FindElement(By.Id("altura")).GetAttribute("value");
            Assert.IsFalse(float.TryParse(altura, out float alturaSalida), "La altura es un float");
            botonGuardarModificado.Click();
            filaMod = driver.FindElements(By.XPath("/html/body/table/tbody/tr"));
            Assert.AreEqual(filaOrig, filaMod[0].Text, "La tabla ha cambiado");

            EliminarUsuariosTests();
        }

        [Test(Author = "Carlos", Description = "Crear usuarios CORRECTOS")]
        public void TestCrearValidos()
        {
            IWebElement tablaPrevia = driver.FindElement(By.Id("tabla-usu"));
            int CountRowsPrevio = tablaPrevia.FindElements(By.TagName("tr")).Count;

            crearVariosUsuarios();
            Recargar();

            IWebElement tablaPost = driver.FindElement(By.Id("tabla-usu"));
            int CountRowsPost = tablaPost.FindElements(By.TagName("tr")).Count;

            Assert.Greater(CountRowsPost, CountRowsPrevio, "La tabla sigue teniendo el mismo n�mero de filas. No se ha a�adido a nadie.");
            EliminarUsuariosTests();
        }

        [Test(Author = "Carlos", Description = "Test eliminar usuarios")]
        public void TestEliminar()
        {
            crearVariosUsuarios();
            Recargar();
            EliminarUsuariosTests();
            IWebElement tablaDespuesVaciar = driver.FindElement(By.Id("tabla-usu"));
            int CountRowsDespuesVaciar = tablaDespuesVaciar.FindElements(By.TagName("tr")).Count;
            Assert.IsTrue(CountRowsDespuesVaciar == 1, "No se ha eliminado a nadie");
            Assert.Throws<NoSuchElementException>(() => driver.FindElement(By.XPath($"html/body/table/tbody/tr[{cuentaOriginal + 1}]")), "No se ha eliminado a nadie");
        }

        [Test(Author = "carlos", Description = "Modificar usuarios BIEN")]
        public void TestModificarValidos()
        {
            crearVariosUsuarios();
            Recargar();

            int indice_a_mod = cuentaOriginal + 1;
            modificaEste(indice_a_mod, "NOMBRE MODIFICADO", 18, 200, true);

            Recargar();

            //COMPROBAR LOS DATOS DEL USUARIO MODIFICADO
            IWebElement celdaNombre = driver.FindElement(By.XPath($"html/body/table/tbody/tr[{indice_a_mod}]/td[1]"));
            IWebElement celdaEdad = driver.FindElement(By.XPath($"html/body/table/tbody/tr[{indice_a_mod}]/td[2]"));
            IWebElement celdaAltura = driver.FindElement(By.XPath($"html/body/table/tbody/tr[{indice_a_mod}]/td[3]"));
            IWebElement celdaActivo = driver.FindElement(By.XPath($"html/body/table/tbody/tr[{indice_a_mod}]/td[4]"));

            Assert.IsTrue(celdaNombre.Text == "NOMBRE MODIFICADO", "No se ha modificado el nombre");
            Assert.IsTrue(int.Parse(celdaEdad.Text) == 18, "No se ha modificado la edad");
            Assert.IsTrue(float.Parse(celdaAltura.Text) == 200, "No se ha modificado la altura");
            Assert.IsTrue(bool.Parse(celdaActivo.Text) == true, "No se ha cambiado el estado de activo");

            EliminarUsuariosTests();
        }

        [Test]
        public void TestRefreshInputs()
        {
            Assert.Multiple(() =>
                {
                    IWebElement elementoNombre = driver.FindElement(By.Id("nombre"));
                    elementoNombre.Clear();
                    elementoNombre.SendKeys("NOMBRE DE PRUEBA EOEOEOEOE");
                    Recargar();
                    elementoNombre = driver.FindElement(By.Id("nombre"));
                    Assert.Zero(elementoNombre.GetAttribute("value").ToString().Length, "El input tiene texto. NO ha hecho refresh 'duro'.");

                    elementoNombre.SendKeys("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAH!!!");
                    driver.FindElement(By.XPath("//body")).SendKeys(Keys.F5);
                    elementoNombre = driver.FindElement(By.Id("nombre"));
                    Assert.NotZero(elementoNombre.GetAttribute("value").ToString().Length, "El reset se ha limpiado. Deber�a tener valores.");
                });
            Recargar(); // Para dejar la p�gina como estaba: controles vacios
        }
        #endregion
    }

}



