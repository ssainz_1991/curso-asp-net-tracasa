class Controlador
{
    listaAux = []
    constructor()
    {
        this.modelo = new Modelo();
        this.modelo.actualizar = ()=> {
            vista.recarga();}
    }

    modificarLlamado(indice, nombreN, edadN, alturaN, activoN)
    {
        let usuNuevo = new Usuario(nombreN, edadN, alturaN, activoN)
        return this.modelo.modificar(usuNuevo, indice)
    }
    
    eliminarLlamado(indice)
    {
        this.modelo.eliminar(indice);
    }

    añadirNuevo(nombre, edad, altura, activo)
    {
        let usu = new Usuario(nombre, edad, altura, activo)
        return this.modelo.crear(usu);
    };

    leerTodos()
    {
        this.listaAux = this.modelo.leerTodos();
        return this.listaAux;
    }

    leer(indice)
    {
        return this.modelo.leer(indice);
    }

    
    
}