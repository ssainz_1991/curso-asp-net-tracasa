class Modelo
{
    listaUsuarios = [];
    actualizar = ()=>{}

    leerTodos()
    {
        let promesaAJAX = fetch("http://localhost:21902/api/usuarios");
        promesaAJAX
                .then((respuesta) => respuesta.json())
                .then(objWF => {
                    for (let wf of objWF) {
                        let usuAux = new Usuario(wf.id,
                                                wf.nombre,
                                                wf.edad,
                                                wf.altura,
                                                wf.activo)
                        this.listaUsuarios.push(usuAux);
                    }
                    this.actualizar();
                })
                .catch(error => {
                    console.error("ERROR en peticion: " + error )
                });
        
        return this.listaUsuarios;
        
        /*if (JSON.parse(window.localStorage.getItem("listaUsuarios")) == null)
        {
            window.localStorage.setItem("listaUsuarios", JSON.stringify([]))
        }
        this.listaUsuarios = JSON.parse(window.localStorage.getItem("listaUsuarios"));
        */
       
       /*alert(this.listaUsuarios.length)*/
    }

    crear(usu)
    {
        if (this.validar(usu))
        {
            this.listaUsuarios.push(usu);
            window.localStorage.setItem("listaUsuarios", JSON.stringify(this.listaUsuarios));
            this.actualizar();
            return true
        }
        else
            return false;
    }

    modificar(usuNuevo, indice)
    {
        if (this.validar(usuNuevo))
        {
            this.listaUsuarios[indice] = usuNuevo
            window.localStorage.setItem("listaUsuarios", JSON.stringify(this.listaUsuarios));
            this.actualizar();
            return true;
        }
        else
            return false;
    }

    eliminar(indice)
    {
        this.listaUsuarios.splice(indice, 1);
        window.localStorage.setItem("listaUsuarios", JSON.stringify(this.listaUsuarios));
        this.actualizar();
    }

    leer(indice) {
        console.log("MÉTODO LEER DEL MODELO, LA LISTA DE USUARIOS TIENE " + this.listaUsuarios.length + " usuarios");
        return this.listaUsuarios[indice];
    }

    validar(usu) {
        if (usu.nombre.length == 0 | isNaN(parseInt(usu.edad)) | isNaN(parseFloat(usu.altura))) {
            console.log("ERROR DE TIPADO DE DATOS: TIPO INCORRECTO DE DATOS O CAMPOS VACÍOS")
            return false;
        }

        if (usu.edad < 0 | usu.altura < 0) {
            console.log("Ni edad ni altura pueden ser menores que 0")
            return false;
        }
        return true;
    } 
}

