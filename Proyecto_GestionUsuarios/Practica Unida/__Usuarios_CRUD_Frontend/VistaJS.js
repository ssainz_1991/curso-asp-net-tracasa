window.onload = function ()
{
    vista = new Vista();
};


//TODO: MOVER CONSTRUCTOR A FUNCIÓN

class Vista
{
    control
    constructor()
    {   
        this.control = new Controlador();
        let listaAux = control.leerTodos();
        this.hazInterfaz(listaAux);
    }
    
    hazInterfaz(listaAux)
    {
       

        //CREAMOS BOTÓN
        let boton = document.getElementById("btn-guardar");
        boton.onclick = () => {
                if (!control.añadirNuevo(document.getElementById("nombre").value,
                    parseInt(document.getElementById("edad").value),
                    parseFloat(document.getElementById("altura").value),
                    document.getElementById("activo").checked))
                    console.log("problema con la validación de datos");
            }

        let numeroUsu = listaAux.length;
    
        //SE HACE LA TABLA
        let body = document.getElementsByTagName("body")[0]

        let tablaUsu = body.appendChild(document.createElement("table"));
        tablaUsu.className = "tabla";
        tablaUsu.id = "tabla-usu";

        //CABECERAS
        let thead = document.createElement("thead");
        thead.innerHTML =
            `<tr>
                <th>Nombre</th>
                <th>Edad</th>
                <th>Altura</th>
                <th>Activo</th>
                <th>Accion</th>
            </tr>`;
            
        tablaUsu.appendChild(thead);
        tablaUsu.children[0].className = "tabla-cabecera";

        //SE RECORRE LA LISTA DE USUARIOS Y SE RELLENA LA TABLA
        if (numeroUsu > 0) {
            let tbody = tablaUsu.appendChild(document.createElement("tbody"));
            for (let indice = 0; indice < numeroUsu; indice++)
            {
                let nodoTexto = tbody.appendChild(document.createElement("tr"))
                    .appendChild(document.createElement("td"))
                    .appendChild(document.createTextNode(listaAux[indice].id));
                let tr = nodoTexto.parentElement.parentElement;
                tr.appendChild(document.createElement("td"))
                tr.appendChild(document.createTextNode(listaAux[indice].nombre));
                tr.appendChild(document.createElement("td"))
                    .appendChild(document.createTextNode(listaAux[indice].edad));
                tr.appendChild(document.createElement("td"))
                    .appendChild(document.createTextNode(listaAux[indice].altura));
                tr.appendChild(document.createElement("td"))
                    .appendChild(document.createTextNode(listaAux[indice].activo));
                

                //BOTONES DE ELIMINAR Y MODIFICAR PARA CADA USUARIO
                let btnElim = document.createElement("button");
                btnElim.innerText = "X";
                btnElim.onclick = () => control.eliminarLlamado(indice);

                let btnMod = document.createElement("button");
                btnMod.innerText = "M"
                btnMod.onclick = () => 
                {
                    document.getElementById("nombre").value = document.getElementById("tabla-usu").rows[indice+1].cells[0].innerText;
                    document.getElementById("edad").value = document.getElementById("tabla-usu").rows[indice+1].cells[1].innerText;
                    document.getElementById("altura").value = document.getElementById("tabla-usu").rows[indice+1].cells[2].innerText;
                    document.getElementById("activo").checked = document.getElementById("tabla-usu").rows[indice+1].cells[3].innerText == 'true';
                    document.getElementById("btn-guardar").value = "Modificar";
                    boton.onclick = () => {

                        if (!control.modificarLlamado(indice,
                            document.getElementById("nombre").value,
                            parseInt(document.getElementById("edad").value),
                            parseFloat(document.getElementById("altura").value),
                            document.getElementById("activo").checked))
                            console.log("PROBLEMA CON LA VALIDACIÓN DE DATOS");

                }}

                let div = document.createElement("div");
                div.appendChild(btnElim)
                div.appendChild(btnMod)
                tr.appendChild(document.createElement("td"))
                .appendChild(div)
            }
        }
    }

    recarga()
    {
        document.getElementById("tabla-usu").remove();
        this.hazInterfaz();
        document.getElementById("nombre").value = "";
        document.getElementById("edad").value = 0;
        document.getElementById("altura").value = 0;
        document.getElementById("activo").checked = true;
        document.getElementById("btn-guardar").value = "Añadir";
    }
}