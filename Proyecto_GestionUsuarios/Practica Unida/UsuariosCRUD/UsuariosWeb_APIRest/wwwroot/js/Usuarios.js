class Usuario
{
    constructor(id, nombre, edad, altura, activo)
    {
        this.id = id;
        this.nombre = nombre;
        this.edad = parseInt(edad);
        this.altura = parseFloat(altura);
        this.activo = activo;
    }
}