class Modelo
{
    listaUsuarios = [];
    actualizar = ()=>{}

    leerTodos()
    {
        this.listaUsuarios = []
        let promesaAJAX = fetch("/api/usuarios");
        promesaAJAX
            .then((respuesta) => respuesta.json())
            .then(objWF => {
                    for (let wf of objWF) {
                        let usuAux = new Usuario(wf.id,
                                                wf.nombre,
                                                wf.edad,
                                                wf.altura,
                                                wf.activo)
                        this.listaUsuarios.push(usuAux);
                }
                    this.actualizar(this.listaUsuarios);
                })
                .catch(error => {
                    console.error("ERROR en peticion: " + error )
                });
        
        return this.listaUsuarios;
        
        /*if (JSON.parse(window.localStorage.getItem("listaUsuarios")) == null)
        {
            window.localStorage.setItem("listaUsuarios", JSON.stringify([]))
        }
        this.listaUsuarios = JSON.parse(window.localStorage.getItem("listaUsuarios"));
        */
    }

    crear(usu)
    {
        if (this.validar(usu))
        {
            fetch('/api/usuarios/crear', {
                headers: {
                    'Content-type': 'application/json'
                },
                method: 'POST',
                body: `{
                            "nombre": "${usu.nombre}",
                            "edad": ${usu.edad},
                            "altura": ${usu.altura},
                            "activo": ${usu.activo}
                        }`
                })
                .then(() =>
                {
                    this.leerTodos()
                })

        }
        else
            return false;
    }

    modificar(usu, indice)
    {
        if (this.validar(usu)) {
            fetch('/api/usuarios/modificar', {
                headers: {
                    'Content-type': 'application/json'
                },
                method: 'PUT',
                body: `{
                            "nombre": "${usu.nombre}",
                            "edad": ${usu.edad},
                            "id": ${usu.id},
                            "altura": ${usu.altura},
                            "activo": ${usu.activo}
                        }`
            })
                .then(() => {
                    this.leerTodos()
                })

        }
        else
            return false;
        /*console.log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
        console.log(usuNuevo.activo);
        //function enviarJSONPut() {
            let opcionesPUT = {
                method: "PUT",
                mode: "cors",
                cache: "no-cache",
                headers: {
                    "Content-type": "application/json"
                },
                body: `{    "nombre": "'${usuNuevo.nombre}'",
                            "edad": "'${usuNuevo.edad}'",
                            "altura":"'${usuNuevo.altura}'",
                            "activo": "'${usuNuevo.activo}'"
                }`
            }

        //}
        

        if (this.validar(usuNuevo))
        {

            // LLAMADA POST, de manera sincrona (esperando respuesta)
            let promesaPUT = fetch("/api/usuarios/modificar",
                opcionesPUT)

            promesaPUT.then((respuesta) => respuesta.json()).then(
                this.leerTodos()
            );
            //this.listaUsuarios[indice] = usuNuevo
           // window.localStorage.setItem("listaUsuarios", JSON.stringify(this.listaUsuarios));
            return true;
        }
        else
            return false;*/
    }

    eliminar(id)
    {
        let promesaAJAX = fetch("/api/usuarios/eliminar/" + id,
            {
                method: "DELETE"
            });
        promesaAJAX
            .then(() => {
                let indice = 0;
                for (let usu in this.listaUsuarios) {
                    if (usu.id === id)
                        break;
                    indice += 1
                }
                this.leerTodos();
            })
            .catch(error => {
                console.error("ERROR en peticion: " + error)
            });
        
        /*this.listaUsuarios.splice(indice, 1);
        window.localStorage.setItem("listaUsuarios", JSON.stringify(this.listaUsuarios));
        this.actualizar();*/
    }

    leer(indice) {
        return this.listaUsuarios[indice];
    }

    validar(usu) {

        if (usu.nombre.length == 0 | isNaN(parseInt(usu.edad)) | isNaN(parseFloat(usu.altura))) {
            console.log("ERROR DE TIPADO DE DATOS: TIPO INCORRECTO DE DATOS O CAMPOS VACÍOS")
            return false;
        }

        if (usu.edad < 0 | usu.altura < 0) {
            console.log("Ni edad ni altura pueden ser menores que 0")
            return false;
        }
        return true;
    } 
}

