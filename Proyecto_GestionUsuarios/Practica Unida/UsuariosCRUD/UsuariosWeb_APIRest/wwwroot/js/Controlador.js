class Controlador
{
    listaAux = []
    constructor()
    {
        this.modelo = new Modelo();
        this.modelo.actualizar = (listaUsuarios)=> {
            vista.recarga(listaUsuarios);}
    }

    modificarLlamado(indice, nombreN, edadN, alturaN, activoN)
    {
        let usuNuevo = new Usuario(indice,nombreN, edadN, alturaN, activoN)
        return this.modelo.modificar(usuNuevo, indice)
    }
    
    eliminarLlamado(indice)
    {
        this.modelo.eliminar(indice);
    }

    añadirNuevo(nombre, edad, altura, activo)
    {
        //el 0 no tiene nunguna razón especifica, es por el constructor.
        let usu = new Usuario(0, nombre, edad, altura, activo) 
        return this.modelo.crear(usu);
    };

    leerTodos()
    {
        this.listaAux = this.modelo.leerTodos();
        return this.listaAux;
    }

    leer(indice)
    {
        return this.modelo.leer(indice);
    }

    
    
}