﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Usuarios_CRUD_WCF.Controllers
{
    // [EnableCors]
    [ApiController]
    [Route("api/[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            var rng = new Random();
            // throw new Exception("Petamos el servidor");
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }

        [HttpPost]
        public WeatherForecast Post(WeatherForecast bodyDelJson)
        {
            //if (bodyDelJson.TemperatureC > 1000)
            //    throw new ArgumentException("Te has pasado de temperatura");

            var rng = new Random();
            bodyDelJson.TemperatureC = bodyDelJson.TemperatureC + 1 + rng.Next(0, 100);
            bodyDelJson.Date = DateTime.Now;
            Console.WriteLine("Funcionó " + bodyDelJson.ToString());
            return bodyDelJson;
        }
    }
}
