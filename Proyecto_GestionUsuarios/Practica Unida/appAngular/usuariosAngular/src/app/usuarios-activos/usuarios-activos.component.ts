import { Component, Input, OnInit } from '@angular/core';
import { ServicioUsuariosService } from '../servicio-usuarios.service';

@Component({
  selector: 'app-usuarios-activos',
  templateUrl: './usuarios-activos.component.html',
  styleUrls: ['./usuarios-activos.component.css']
})
export class UsuariosActivosComponent implements OnInit {

  /*@Input()
  activos: string = "0";
  @Input()
  inactivos: string = "0";*/

  constructor(public srvUsu: ServicioUsuariosService) { }

  ngOnInit(): void {
  }

}
