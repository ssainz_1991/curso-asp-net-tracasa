import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
import { Usuarios } from './usuarios';


@Injectable({
  providedIn: 'root'
})
export class ServicioUsuariosService {

  private arrayUsuarios: Array<Usuarios> = [];
  private url: string = "http://localhost:1349/api/usuarios";


  constructor(private clienteHTTP: HttpClient) { 
    let observ: Observable<Array<Usuarios>>; 
    observ = this.clienteHTTP.get<Array<Usuarios>>(this.url);
    observ.subscribe( (datos: Array<Usuarios>) => {
      this.arrayUsuarios = datos;
      this.contarAdultosPorSrv();
      this.contarActivos();
    });

  }

  public LeerUsuarios() :Array<Usuarios>{

    return this.arrayUsuarios;

  }

  public contarAdultosPorSrv(): Array<number> {

    let valor1: number = 0;
    let valor2: number = 0;
    let res:Array<number> = [];

    /*for (const i of arrayUsuarios) {*/
    for(let i = 0; i < this.arrayUsuarios.length; i++) {
      if(this.arrayUsuarios[i].edad>17){
        valor1++;
      }
      else{
        valor2++;
      }
    }
    res.push(valor1);
    res.push(valor2);
    return res;
  } 
  
  public contarActivos (): Array<number> {
    
    let valor1: number = 0;
    let valor2: number = 0;
    let res:Array<number> = [];

    for(let i = 0; i < this.arrayUsuarios.length; i++) {
      if(this.arrayUsuarios[i].activo){
        valor1++;
      }
      else{
        valor2++;
      }
    }
    res.push(valor1);
    res.push(valor2);
    return res;
  } 

}
