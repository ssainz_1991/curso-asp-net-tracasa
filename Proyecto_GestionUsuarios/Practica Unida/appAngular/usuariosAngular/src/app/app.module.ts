import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UsuariosActivosComponent } from './usuarios-activos/usuarios-activos.component';
import { UsuariosEdadComponent } from './usuarios-edad/usuarios-edad.component';
import { HttpClientModule } from '@angular/common/http';
import { ServicioUsuariosService } from './servicio-usuarios.service';



@NgModule({
  declarations: [
    AppComponent,
    UsuariosActivosComponent,
    UsuariosEdadComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [ServicioUsuariosService],
  bootstrap: [AppComponent]
})
export class AppModule { }

